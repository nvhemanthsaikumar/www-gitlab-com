---
layout: handbook-page-toc
title: "GitLab Security Compliance Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Security Controls

Security controls are a way to state our company's position on a variety of security topics. It's not enough to simply say "We encrypt data" since our customers and teams will naturally want to know "what data do we encrypt?" and "how do we encrypt that data?". When all of our established security controls are operating effectively this creates a security program greater than the sum of its parts that will demonstrate to our stakeholders that GitLab has a mature and comprehensive security program that will provide assurance that data within GitLab is reasonably protected.

## GitLab Control Framework (GCF)

We have tried to take a comprehensive approach to our immediate and future security compliance needs. Older and larger companies tend to treat each security compliance requirement individually which results in independent security compliance teams going out to internal teams with multiple overlapping requests. For example, at such a company you might have one database engineer that is asked to provide evidence of how a particular database is encrypted based on SOC2 requirements, then again for ISO requirements, then again for FedRAMP requirements. This approach can be visualized as follows:

```mermaid
graph TD;
    SOC2_Requirement1-->Team1;
    SOC2_Requirement1-->Team2;
    SOC2_Requirement2-->Team1;
    SOC2_Requirement2-->Team2;
    FedRAMP_Requirement1-->Team1;
    ISO_Requirement1-->Team2;
```

Given our [efficiency value](/handbook/values/#efficiency) here at GitLab we wanted to create a set of security controls that would address multiple underlying requirements with a single security control which would allow us to make fewer requests of our internal teams and efficiently collect all evidence we would need for a variety of audits at once. This approach can be visualized as follows:

```mermaid
graph TD;
    SOC2_Requirement1-->GCF;
    SOC2_Requirement2-->GCF;
    FedRAMP_Requirement1-->GCF;
    ISO_Requirement1-->GCF;
    GCF-->Team1;
    GCF-->Team2;
```

As our security compliance goals and requirements have evolved so have our requirements and constraints related to our security control framework. The latest iteration of our GCF is based on the [Secure Control Framework by ComplianceForge](https://www.complianceforge.com/scf/) since the SCF comes with extensive and reliable compliance and regulatory framework mappings.

## Security Control Lifecycle

The lifecycle of our security controls can be found at [this handbook page](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html). As part of the security control lifecycle, all GCF security controls are reviewed and tested at a minimum on an annual basis or as required by regulation.

## Control Ownership

Control Owner - Ensures that the design of the control and the control activities operate effectively and is responsible for remediation of any control activities that are required to bring that control into a state of audit-readiness.

Process Owner - Supports the operation of the control and carries out the process designed by the control owner. The process owner is most likely to be interviewed by an auditor to determine whether or not the process is operating as intended.

## Security Control Changes

The GitLab compliance team is responsible for ensuring the consistency of the documentation of the security controls listed below. While normally we welcome any GitLab team-member to make edits to handbook pages, please be aware that even small changes to the wording of any of these controls impacts how they satisfy the requirements for the security frameworks they map to. Because of this, we ask any changes that need to be made to this page and the underlying guidance pages to start with a comment in [this issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/219). The compliance team will then engage with you and make any appropriate changes to these handbook pages.

# List of controls by family:
*We are continuing to update the controls with additional details*

<details markdown="1">
<summary>Asset Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| AST-04 | Network Diagrams & Data Flow Diagrams (DFDs) | Maintain network architecture diagrams that:<br> - Contain sufficient detail to assess the security of the network's architecture; <br>- Reflect the current state of the network environment; <br> - Document all sensitive data flows. |
| AST-09 | Secure Disposal or Re-Use of Equipment | Securely destroy media when it is no longer needed for business or legal purposes. |

</summary>
</details>

<details markdown="1">
<summary>Business Continuity & Disaster Recovery</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| BCD-01 | Business Continuity Management System (BCMS) | Facilitate contingency planning security controls to help ensure resilient assets and services. |
| BCD-02 | Identify Critical Assets | Identify, document and resume the critical systems, applications and services that support essential missions and business processes within Recovery Time Objectives (RTOs) with little or no loss of operational continuity of the defined time period of the contingency plan’s activation. |
| BCD-04 | Contingency Plan Testing & Exercises | Conduct tests and/or exercises to determine the contingency plan’s effectiveness and the organization’s readiness to execute the plan. |
| BCD-05 | Contingency Plan Root Cause Analysis (RCA) & Lessons Learned | Conduct Root Cause Analysis (RCA) and “lessons learned” activities every time the contingency plan is activated. |
| BCD-06 | Contingency Planning & Updates | Keep contingency plans current with business requirements and technology changes. |
| BCD-07 | Alternative Security Measures | Alternative or compensating controls to satisfy security requirements when the primary means of implementing the security requirements is unavailable or compromised. |
| BCD-11 | Data Backups | Create and routinely test recurring backups of data, software and system images verifying the reliability of the backup process to ensure the integrity and availability of the data. |
| BCD-12 | Information System Recovery & Reconstitution | Ensure the recovery and restoration of systems to a known state after a disruption, compromise or failure. |
| BCD-13 | Backup & Restoration Hardware Protection | Protect backup and restoration of hardware and software. |

</details>

<details markdown="1">
<summary>Configuration Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| CFG-01 | Configuration Management Program | Facilitate the implementation of configuration management security controls. |
| CFG-02 | System Hardening Through Baseline Configurations | Develop, document, review, update and maintain secure baseline configurations at least annually as part of system component installations and upgrades for technology platforms that are consistent with industry-accepted system hardening standards and automating reports on baseline configurations of the systems. |

</details>

<details markdown="1">
<summary>Change Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| CHG-01 | Change Management Program | Facilitate the implementation of change management security controls. |
| CHG-02 | Configuration Change Control | Govern the technical configuration change control processes by testing and documenting proposed changes in a non-production environment before changes are implemented in a production environment including a cybersecurity representative in the configuration change control review process. |
| CHG-03 | Security Impact Analysis for Changes | Analyze proposed changes for potential security impacts, prior to the implementation of the change. |
| CHG-04 | Access Restriction For Change | Enforce configuration restrictions in an effort to restrict the ability of users to conduct unauthorized changes. |
| CHG-05 | Stakeholder Notification of Changes | Ensure stakeholders are made aware of and understand the impact of proposed changes. |
| CHG-06 | Security Functionality Verification | Verify the functionality of security controls when anomalies are discovered. |

</details>

<details markdown="1">
<summary>Cloud Security</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| CLD-01 | Cloud Services | Facilitate the implementation of cloud management security controls to ensure cloud instances are secure and in-line with industry best practices. |

</details>

<details markdown="1">
<summary>Compliance</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| CPL-01 | Statutory, Regulatory & Contractual Compliance | Facilitate the identification and implementation of relevant legislative statutory, regulatory and contractual security controls. |
| CPL-02 | Security Controls Oversight | Responsible for security controls oversight. |
| CPL-03 | Security Assessments | Ensure team members regularly review controlled documents within their area of responsibility for accuracy and adherence to appropriate security policies, standards and other applicable requirements. |
| CPL-04 | Audit Activities | Plan and execute compliance audits that minimize the impact of audit activities on business operations. |

</details>

<details markdown="1">
<summary>Cryptographic Protections</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| CRY-01 | Use of Cryptographic Controls | Facilitate the implementation of cryptographic protections security controls using known public standards and trusted cryptographic technologies. |
| CRY-03 | Transmission Confidentiality | Cryptographic mechanisms to protect the confidentiality of data being transmitted. |
| CRY-05 | Encrypting Data At Rest | Cryptographic mechanisms to prevent unauthorized disclosure of information at rest. |
| CRY-08 | Public Key Infrastructure (PKI) | Implement an internal Public Key Infrastructure (PKI) or obtain PKI services from a reputable PKI service provider. |
| CRY-09 | Cryptographic Key Management | Controls to protect the confidentiality, integrity and availability of keys by facilitating the production and management of symmetric cryptographic keys using Federal Information Processing Standards (FIPS)-compliant and asymmetric cryptographic keys using approved key management technology and processes that protect the user’s private key. |

</details>

<details markdown="1">
<summary>Data Classification & Handling</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| DCH-01 | Data Protection | Facilitate logical and physical data protection controls and ensure data stewardship is assigned, documented and communicated and the quality of information remains complete and verifiable. |
| DCH-02 | Data & Asset Classification | Ensure a complete and accurate data and asset list are categorized and prioritized based on their classification, criticality and business value, in accordance with applicable statutory, regulatory and contractual requirements. |
| DCH-08 | Physical Media Disposal | Securely retain and dispose of physical media when it is no longer required, using formal procedures. |
| DCH-09 | Digital Media Sanitization | Sanitize media, both digital and non-digital, with the strength and integrity commensurate with the classification or sensitivity of the information prior to disposal, release out of organizational control or release for reuse. |
| DCH-10 | Media Use | Restrict the use of some types of digital media on systems or system components. |
| DCH-12 | Removable Media Security | Restrict removable media in accordance with data handling and acceptable usage parameters. |
| DCH-13 | Use of External Information Systems| Restrict the use of portable storage devices by external parties, systems and services used to securely store, process and transmit data. |
| DCH-14 | Information Sharing | Utilize a process assisting users in making information sharing decisions to ensure data is appropriately protected. |
| DCH-17 | Ad-Hoc Transfers | Secure ad-hoc exchanges of large digital files with internal or external parties. |
| DCH-21 | Information Disposal | Securely dispose of, destroy or erase information. |
| DCH-22 | Data Quality Operations | Check for the accuracy, relevance, timeliness, impact, completeness and de-identification of information across the information lifecycle. |

</details>

<details markdown="1">
<summary>Endpoint Security</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| END-04 | Malicious Code Protection (Anti-Malware) | Utilize anti-malware technologies to detect and eradicate malicious code. |
| END-06 | File Integrity Monitoring (FIM) | Utilize File Integrity Monitor (FIM) technology to detect and report unauthorized changes to system files and configurations. |
| END-07 | Host Intrusion Detection and Prevention Systems (HIDS / HIPS) | Utilize Host-based Intrusion Detection / Prevention Systems (HIDS / HIPS) on sensitive systems. |

</details>

<details markdown="1">
<summary>Security & Privacy Governance</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| GOV-01 | Security & Privacy Governance Program | Facilitate cybersecurity and privacy governance security controls. |
| GOV-02 | Publishing Security & Privacy Documentation | Establish, maintain and disseminate cybersecurity and privacy policies, standards and procedures. |
| GOV-03 | Periodic Review & Update of Security & Privacy Program | Review the cybersecurity and privacy program, including policies, standards and procedures, at planned intervals or if significant changes occur to ensure their continuing suitability, adequacy and effectiveness. |
| GOV-04 | Assigned Security & Privacy Responsibilities | Assign a qualified individual with the mission and resources to centrally-manage, coordinate, develop, implement and maintain an enterprise-wide cybersecurity and privacy program. |
| GOV-05 | Measures of Performance | Implementing Key Performance Indicators (KPIs) assisting organizational management and Key Risk Indicators (KRIs) assisting senior management with developing, reporting and monitoring measures of performance and trend analysis of the cybersecurity and privacy program. |
| GOV-06 | Contacts With Authorities | Identify and document appropriate contacts within relevant law enforcement and regulatory bodies. |

</details>

<details markdown="1">
<summary>Human Resources Security</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| HRS-01 | Human Resources Security Management | Facilitate personnel security controls. |
| HRS-02 | Position Categorization | Manage personnel security risk by assigning a risk designation to all positions and establishing screening criteria for individuals filling those positions. |
| HRS-03 | Roles & Responsibilities | Define cybersecurity responsibilities for all personnel and ensure that all security-related positions are staffed by qualified individuals who have the necessary skill set. |
| HRS-04 | Personnel Screening | Manage personnel security risk and formally indoctrinate all the relevant types of information to which an individual would have access to by following organized-defined special protections of screening individuals prior to authorizing access to a system that stores, transmits or processes sensitive information. |
| HRS-05 | Terms of Employment | Requiring all employees and contractors to apply security and privacy principles in their daily work defined by acceptable and unacceptable rules of behavior for the use of technologies, including consequences for unacceptable behavior. |
| HRS-06 | Access Agreements | Require employees and third-party users to sign appropriate access agreements such as Non-Disclosure Agreements (NDAs) or similar confidentiality agreements that reflect the needs to protect data and operational details prior to being granted access. |
| HRS-07 | Personnel Sanctions | Sanction personnel failing to comply with established security policies, standards and procedures by conducting employee misconduct investigations when there is reasonable assurance that a policy has been violated. |
| HRS-08 | Personnel Transfer | Adjust logical and physical access authorizations to systems and facilities upon personnel reassignment or transfer, in a timely manner. |
| HRS-09 | Personnel Termination | Govern the termination of individual employment by: <br> -cretrieving organization-owned assets upon termination, <br> - expediting the process of removing "high risk" individual’s access to systems and applications upon termination, as determined by management; and <br> - governing third-party personnel by notifying terminated individuals of applicable, legally binding post-employment requirements for the protection of organizational information. |
| HRS-10 | Third-Party Personnel Security | Govern third-party personnel by reviewing and monitoring third-party cybersecurity and privacy roles and responsibilities. |
| HRS-11 | Separation of Duties | Maintain Separation of Duties (SoD) to prevent potential malevolent activity without collusion. |
| HRS-13 | Identify Critical Skills & Gaps | Evaluate the critical cybersecurity and privacy skills needed to support the organization’s mission and identify gaps that exist. |

</details>

<details markdown="1">
<summary>Identification & Authentication</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| IAC-01 | Identity & Access Management (IAM) | Facilitate the identification and access management security controls. |
| IAC-02 | Identification & Authentication for Organizational Users | Uniquely identify and authenticate organizational users and processes acting on behalf of organizational users. |
| IAC-03 | Identification & Authentication for Non-Organizational Users | Uniquely identify and authenticate third-party users and processes that provide services to the organization. |
| IAC-04 | Identification & Authentication for Devices | Uniquely identify and authenticate devices before establishing a connection using bidirectional authentication that is cryptographically- based and replay resistant. |
| IAC-05 | Identification & Authentication for Third Party Systems & Services | Identify and authenticate third-party systems and services. |
| IAC-06 | Multi-Factor Authentication (MFA) | Automatically enforce Multi-Factor Authentication (MFA) for: <br> - Remote network access; and/or <br> - Non-console access to critical systems or systems that store, transmit and/or process sensitive data. |
| IAC-07 | User Provisioning & De-Provisioning | Utilize a formal user registration and de-registration process that governs the assignment of access rights. |
| IAC-07.1 | Change of Roles & Duties | Revoke user access rights following changes in personnel roles and duties, if no longer necessary or permitted. |
| IAC-08 | Role-Based Access Control (RBAC) | Enforce a Role-Based Access Control (RBAC) policy over users and resources that applies need-to-know and fine-grained access control for sensitive data access. |
| IAC-09 | Identifier Management (User Names) | Govern naming standards for usernames and systems to ensure proper user identification management for non-consumer users and administrators. |
| IAC-10 | Authenticator Management (Passwords) | Securely manage passwords for users and devices ensuring vendor-supplied defaults are changed as part of the installation process. |
| IAC-15 | Account Management | Proactively govern account management of individual, group, system, application, guest and temporary accounts. |
| IAC-16 | Privileged Account Management (PAM) | Restrict and control privileged access rights for users and services. |
| IAC-17 | Periodic Review | Periodically review the privileges assigned to users to validate the need for such privileges; and reassign or remove privileges, if necessary, to correctly reflect organizational mission and business needs. |
| IAC-20 | Access Enforcement | Eenforce logical access permissions through the principle of "least privilege." |
| IAC-21 | Least Privilege | Utilize the concept of least privilege, allowing only authorized access to processes necessary to accomplish assigned tasks in accordance with organizational business functions. |
| IAC-22 | Account Lockout | Enforce a limit for consecutive invalid login attempts by a user during an organization-defined time period and automatically locks the account when the maximum number of unsuccessful attempts is exceeded. |

</details>

<details markdown="1">
<summary>Information Assurance</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| IAO-01 | Information Assurance (IA) Operations | Facilitate the implementation of cybersecurity and privacy assessment and authorization security controls. |
| IAO-02 | Assessments | Formally assess and ensure assessors or assessment teams have the appropriate independence to conduct cybersecurity and privacy security control assessments in systems, applications and services through Information Assurance Program (IAP) activities to determine the extent to which the security controls are implemented correctly, operating as intended and producing the desired outcome with respect to meeting expected requirements for: <br> - Statutory, regulatory and contractual compliance obligations; <br> - Monitoring capabilities; <br> - Mobile devices; <br> - Databases; <br> - Application security; <br> - Embedded technologies (e.g., IoT, OT, etc.); <br> - Vulnerability management; <br> - Malicious code; <br> - Insider threats; and <br> - Performance/load testing. |
| IAO-04 | Threat Analysis & Flaw Remediation During Development | Require system developers and integrators to create and execute a Security Test and Evaluation (ST&E) plan to identify and remediate flaws during development. |
| IAO-05 | Plan of Action & Milestones (POA&M) | Generate a Plan of Action and Milestones (POA&M), or similar risk register, to document planned remedial actions to correct weaknesses or deficiencies noted during the assessment of the security controls and to reduce or eliminate known vulnerabilities. |
| IAO-06 | Technical Verification | Perform Information Assurance Program (IAP) activities to evaluate the design, implementation and effectiveness of technical security and privacy controls. |

</details>

<details markdown="1">
<summary>Incident Response</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| IRO-01 | Incident Response Operations | Implement and govern processes and documentation to facilitate an organization-wide response capability for security and privacy-related incidents. |
| IRO-02 | Incident Handling | Cover the preparation, automated detection or intake of incident reporting, analysis, containment, eradication and recovery. |
| IRO-04 | Incident Response Plan (IRP) | Maintain and make available a current and viable Incident Response Plan (IRP) to all stakeholders to address Personal Data (PD) incidents according to applicable laws, regulations and contractual obligations. |
| IRO-07 | Integrated Security Incident Response Team (ISIRT) | Establish an integrated team of cybersecurity, IT and business function representatives that are capable of addressing cybersecurity and privacy incident response operations. | 
| IRO-09 | Situational Awareness For Incidents | Document, monitor and report the status of cybersecurity and privacy incidents to internal stakeholders all the way through the resolution of the incident. |
| IRO-10 | Incident Stakeholder Reporting | Report sensitive data incidents and provide security and privacy incident information in a timely manner to applicable: <br> - Internal stakeholders; <br> - Affected clients & third-parties; <br> - Regulatory authorities; and <br> to the provider of the product or service and other organizations involved in the supply chain for systems or system components related to the incident. |
| IRO-14 | Regulatory & Law Enforcement Contacts | Maintain incident response contacts with applicable regulatory and law enforcement agencies. |

</details>

<details markdown="1">
<summary>Mobile Device Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| MDM-01 | Centralized Management of Mobile Devices | Develop, govern & update procedures to facilitate the implementation of mobile device management controls. |
| MDM-03 | Full Device & Container-Based Encryption | Implement cryptographic mechanisms to protect the confidentiality and integrity of information on mobile devices through full-device or container encryption. |

</details>

<details markdown="1">
<summary>Continuous Monitoring</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| MON-01 | Continuous Monitoring | Enterprise-wide monitoring controls such as Intrusion Detection / Prevention Systems (IDS / IPS) technologies on critical systems, key network segments and network choke points. <br> GitLab utilizes Host-based Intrusion Detection / Prevention Systems (HIDS/HIPS) to continuously monitor inbound and outbound communications traffic for unusual or unauthorized activities and actively responds to alerts from physical, cybersecurity, privacy and supply chain activities, blocking unwanted activities to achieve and maintain situational awareness. <br> GitLab utilizes Wireless Intrusion Detection / Protection Systems (WIDS / WIPS) to identify rogue wireless devices and detect attack attempts via wireless networks. <br> GitLab sends logs to a Security Incident Event Manager (SIEM) or similar automated tool to review event logs on an ongoing basis and escalate incidents in accordance with established timelines and procedures. |
| MON-02 | Centralized Collection of Security Event Logs | Utilize a Security Incident Event Manager (SIEM) or similar automated tool, to support the centralized collection of security-related event logs to maintain situational awareness. |
| MON-03 | Content of Audit Records | Configure systems to produce audit records that contain sufficient information to, at a minimum: <br> - Establish what type of event occurred; <br> - When (date and time) the event occurred; <br> - Where the event occurred; <br> - The source of the event; <br> - The outcome (success or failure) of the event; and <br> - The identity of any user/subject associated with the event. |  
| MON-06 | Monitoring Reporting | Provide an event log report generation capability to aid in detecting and assessing anomalous activities. |
| MON-10 | Audit Record Retention | Retain audit records for a time period consistent with records retention requirements to provide support for after-the-fact investigations of security incidents and to meet statutory, regulatory and contractual retention requirements. |
| MON-16 | Anomalous Behavior | Detect and respond to anomalous behavior that could indicate account compromise or other malicious activities. |

</details>

<details markdown="1">
<summary>Network Security</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| NET-01 | Network Security Management | Develop, govern & update procedures to facilitate the implementation of network security controls. |
| NET-02 | Layered Network Defenses | Implement security functions as a layered structure that minimizes interactions between layers of the design and avoiding any dependence by lower layers on the functionality or correctness of higher layers. |
| NET-03 | Boundary Protection | Limit network access points by monitoring and controlling communications at the external network boundary and at key internal boundaries within the network. |
| NET-04 | Data Flow Enforcement – Access Control Lists (ACLs) | Design, implement and review firewall and router configurations to restrict connections between untrusted networks and internal systems and deny network traffic by default and allow network traffic by exception (e.g., deny all, permit by exception). |
| NET-06 | Network Segmentation | Logically or physically segment information flows to accomplish network segmentation to other components of the system and implementing security management subnets to isolate security tools. |
| NET-08 | Network Intrusion Detection / Prevention Systems (NIDS / NIPS) | Employ Network Intrusion Detection / Prevention Systems (NIDS/NIPS) to detect and/or prevent intrusions into the network. |
| NET-12 | Safeguarding Data over Open Networks | Implement cryptographic mechanisms for strong cryptography and security protocols to safeguard sensitive data during transmission over open, public networks and to protect external and internal wireless links from signal parameter attacks through monitoring for unauthorized wireless connections, including scanning for unauthorized wireless access points and taking appropriate action, if an unauthorized connection is discovered. |
| NET-13 | Electronic Messaging | Protect information involved in electronic messaging communications. |
| NET-14 | Remote Access | Define, control and review remote access methods. |

</details>

<details markdown="1">
<summary>Security Operations</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| OPS-01 | Operations Security | Implement operational security controls to identify and document Standardized Operating Procedures (SOP), or similar documentation, to enable the proper execution of day-to-day / assigned tasks. |
| OPS-02 | Security Concept Of Operations (CONOPS) | Develop a security Concept of Operations (CONOPS), or a similarly-defined plan for achieving cybersecurity objectives, that documents management, operational and technical measures implemented to apply defense-in-depth techniques that is communicated to all appropriate stakeholders. |
| OPS-03 | Service Delivery (Business Process Support) | Define supporting business processes and implement appropriate governance and service management to ensure appropriate planning, delivery and support of the organization's technology capabilities supporting business functions, workforce, and/or customers based on industry-recognized standards to achieve the specific goals of the process area. |

</details>

<details markdown="1">
<summary>Physical & Environmental Security</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| PES-01 | Physical & Environmental Protections | Facilitate the operation of physical and environmental protection controls. |
| PES-02 | Physical Access Authorizations | Implement physical access controls to maintain a current list of personnel with authorized access to organizational facilities based on the position or role of the individual, (except for those areas within the facility officially designated as publicly accessible). |
| PES-03 | Physical Access Control | Implement physical access controls to enforce physical access authorizations for all physical access points (including designated entry/exit points) to facilities (excluding those areas within the facility officially designated as publicly accessible). |

</details>

<details markdown="1">
<summary>Privacy</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| PRI-05 | Use, Retention & Disposal | <br> - Retain Personal Data (PD), including metadata, for an organization-defined time period to fulfill the purpose(s) identified in the notice or as required by law; <br> - Dispose of, destroys, erases, and/or anonymizes the PI, regardless of the method of storage; and <br> - Use organization-defined techniques or methods to ensure secure deletion or destruction of PD (including originals, copies and archived records). |
| PRI-14 | Privacy Records & Reporting | Maintain privacy-related records and develop, disseminate and update reports to internal senior management, as well as external oversight bodies, as appropriate, to demonstrate accountability with specific statutory and regulatory privacy program mandates. |

</details>

<details markdown="1">
<summary>Project & Resource Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| PRM-01 | Security Portfolio Management | Security and privacy-related resource planning controls that define a viable plan for achieving cybersecurity & privacy objectives. |
| PRM-02 | Security & Privacy Resource Management | Address all capital planning and investment requests, including the resources needed to implement the security & privacy programs and documents all exceptions to this requirement. |
| PRM-03 | Allocation of Resources | Identify and allocate resources for management, operational, technical and privacy requirements within business process planning for projects / initiatives. |
| PRM-04 | Security & Privacy In Project Management | Assess security and privacy controls in system project development to determine the extent to which the controls are implemented correctly, operating as intended and producing the desired outcome with respect to meeting the requirements. |
| PRM-05 | Security & Privacy Requirements Definition | Identify critical system components and functions by performing a criticality analysis for critical systems, system components or services at pre-defined decision points in the Secure Development Life Cycle (SDLC). |
| PRM-06 | Business Process Definition | Define business processes with consideration for cybersecurity and privacy that determines: <br> - The resulting risk to organizational operations, assets, individuals and other organizations; and <br> - Information protection needs arising from the defined business processes and revises the processes as necessary, until an achievable set of protection needs is obtained. |
| PRM-07 | Secure Development Life Cycle (SDLC) Management | Ensure changes to systems within the Secure Development Life Cycle (SDLC) are controlled through formal change control procedures. |

</details>

<details markdown="1">
<summary>Risk Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| RSK-01 | Risk Management Program | Implement risk management controls. |
| RSK-02 | Risk-Based Security Categorization | Categorizes systems and data in accordance with applicable local, state and Federal laws that: <br> - Document the security categorization results (including supporting rationale) in the security plan for systems; and <br> - Ensure the security categorization decision is reviewed and approved by the asset owner. | 
| RSK-03 | Risk Identification | Identify and document risks, both internal and external. | 
| RSK-04 | Risk Assessment | Conduct an annual assessment of risk and maintain a risk register that monitors the reporting of risks including the likelihood and magnitude of harm, from unauthorized access, use, disclosure, disruption, modification or destruction of the organization's systems and data. | 
| RSK-05 | Risk Ranking | Identify and assign a risk ranking to newly discovered security vulnerabilities that is based on industry-recognized practices. | 
| RSK-06 | Risk Remediation | Remediate risks to an acceptable level and respond to findings from security and privacy assessments, incidents and audits to ensure proper remediation has been performed. |
| RSK-07 | Risk Assessment Update | Routinely update risk assessments and react accordingly upon identifying new security vulnerabilities, including using outside sources for security vulnerability information. |
| RSK-08 | Business Impact Analysis (BIA) | Conduct a Business Impact Analysis (BIA). | 
| RSK-09 | Supply Chain Risk Management Plan | Develop a plan for Supply Chain Risk Management (SCRM) and periodically assess supply chain risks associated with the development, acquisition, maintenance and disposal of systems, system components and services, including documenting selected mitigating actions and monitoring performance against those plans. | 
| RSK-10 | Data Protection Impact Assessment (DPIA) | Conduct a Data Protection Impact Assessment (DPIA) on systems, applications and services to evaluate privacy implications. |

</details>

<details markdown="1">
<summary>Security Awareness & Training</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| SAT-01 | Security & Privacy-Minded Workforce | Security workforce development and awareness controls. |

</details>

<details markdown="1">
<summary>Secure Engineering & Architecture</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| SEA-01 | Secure Engineering Principles | Centrally-manage organization-wide management and implementation of industry-recognized cybersecurity and privacy practices and other related processes in the specification, design, development, implementation and modification of systems and services. |
| SEA-02 | Alignment With Enterprise Architecture | Develop an enterprise architecture, aligned with industry-recognized leading practices, with consideration for cybersecurity and privacy principles that addresses risk to organizational operations, assets, individuals, and other organizations. |

</details>

<details markdown="1">
<summary>Technology Development & Acquisition</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| TDA-01 | Technology Development & Acquisition | Tailored development and acquisition strategies, contract tools and procurement methods to meet unique business needs. |
| TDA-02 | Security Requirements | Technical and functional specifications, explicitly or by reference, in system acquisitions based on an assessment of risk. |
| TDA-15 | Developer Threat Analysis & Flaw Remediation | Require system developers and integrators to create a Security Test and Evaluation (ST&E) plan and implement the plan under the witness of an independent party. |

</details>

<details markdown="1">
<summary>Threat Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| THR-01 | Threat Intelligence Program | Includes a cross-organization information-sharing capability that can influence the development of the system and security architectures, selection of security solutions, monitoring, threat hunting, response and recovery activities. |
| THR-02 | Indicators of Exposure (IOE) | Develop Indicators of Exposure (IOE) to understand the potential attack vectors that could be used to attack the organization. |
| THR-04 | Insider Threat Program | Includes a cross-discipline insider threat incident handling team. |

</details>

<details markdown="1">
<summary>Third-Party Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| TPM-01 | Third-Party Management | Management of third-party security controls. |
| TPM-02 | Third-Party Criticality Assessments | Identify, prioritize and assess suppliers and partners of critical systems, components and services using a supply chain risk assessment process relative to their importance in supporting the delivery of high-value services. |
| TPM-03 | Supply Chain Protection | Evaluating security risks and addressing identified weaknesses or deficiencies in the security associated with the services and product supply chain, limiting harm from potential adversaries who identify and target the organization’s supply chain by utilizing tailored acquisition strategies, contract tools and procurement methods for the purchase of unique systems, system components or services. |
| TPM-04 | Third-Party Services | Mitigate the risks associated with third-party access to the organization’s systems and data and ensure that the interests of third-party service providers are consistent with and reflect organizational interests. |
| TPM-05 | Third-Party Contract Requirements | Identify, regularly review and document third-party confidentiality, Non-Disclosure Agreements (NDAs) and other contracts that reflect the organization’s needs to protect systems and data. |
| TPM-06 | Third-Party Personnel Security | Control personnel security requirements including security roles and responsibilities for third-party providers. |
| TPM-07 | Monitoring for Third-Party Information Disclosure | Monitor for evidence of unauthorized exfiltration or disclosure of organizational information. |
| TPM-08 | Review of Third-Party Services | Monitor, regularly review and audit supplier service delivery for compliance with established contract agreements. |
| TPM-09 | Third-Party Deficiency Remediation | Address weaknesses or deficiencies in supply chain elements identified during independent or organizational assessments of such elements. |
| TPM-10 | Managing Changes To Third-Party Services | Control changes to services by suppliers, taking into account the criticality of business information, systems and processes that are in scope by the third-party. |
| TPM-11 | Third-Party Incident Response & Recovery Capabilities | Ensure response/recovery planning and testing are conducted with critical suppliers/providers. |

</details>

<details markdown="1">
<summary>Vulnerability & Patch Management</summary>

| Control | Description | Purpose/Scope |
|---------|-------------|---------------|
| VPM-02 | Vulnerability Remediation Process | Ensure that vulnerabilities are properly identified, tracked and remediated. |
| VPM-04 | Continuous Vulnerability Remediation Activities | Address new threats and vulnerabilities on an ongoing basis and ensure assets are protected against known attacks. |
| VPM-05 | Software Patching | Conduct software patching for all deployed operating systems, applications and firmware. |
| VPM-06 | Vulnerability Scanning | Detect vulnerabilities and configuration errors by recurring vulnerability scanning of systems and web applications. |
| VPM-07 | Penetration Testing | Conduct penetration testing on systems and web applications. |

</details>

# Legacy GCF
*This information will soon be deprecated.  Please bear with us as we work through cleaing up the links and replacing with new ones.*

## Asset Management
* Device and Media Inventory
    * [AM.1.01 - Inventory Management](./guidance/am.1.01-inventory-management.html)

## Backup Management
* Backup
    * [BU.1.01 - Backup Configuration](./guidance/BU.1.01_backup_configuration.html)
    * [BU.1.02 - Resilience Testing](./guidance/BU.1.02_resilience_testing.html)
    * [BU.1.03 - Alternate Storage](./guidance/BU.1.03_alternate_storage.html)

## Business Continuity
* Business Continuity Planning
    * [BC.1.01 - Business Continuity Plan](./guidance/BC.1.01_business_continuity_plan.html)
    * [BC.1.02 - Business Continuity Plan: Roles and Responsibilities](./guidance/BC.1.02_business_continuity_roles_responsibilities.html)
    * [BC.1.03 - Continuity Testing](./guidance/BC.1.03_continuity_testing.html)
    * [BC.1.04 - Business Impact Analysis](./guidance/BC.1.04_business_impact_analysis.html)

## Change Management
* Change Management
    * [CM.1.01 - Change Management Workflow](./guidance/CM.1.01_change_management_workflow.html)
    * [CM.1.02 - Change Approval](./guidance/CM.1.02_change_approval.html)
    * [CM.1.03 - Change Management Issue Tracker](./guidance/CM.1.03_change_management_issue_tracker.html)
    * [CM.1.04 - Emergency Changes](./guidance/CM.1.04_emergency_changes.html)
* Segregation of Duties
    * [CM.2.01 - Segregation of Duties](./guidance/CM.2.01_segregation_of_duties.html)

## Configuration Management
* Baseline Configurations
    * [CFG.1.01 - Baseline Configuration Standard](./guidance/CFG.1.01_baseline_configuration_standard.html)
    * [CFG.1.03 - Configuration Checks](./guidance/CFG.1.03_configuration_checks.html)

## Data Management
* Data Classification
    * [DM.1.01 - Data Classification Criteria](./guidance/DM.1.01_data_classification_criteria.html)
* Choice and Consent
    * [DM.2.01 - Terms of Service](./guidance/DM.2.01_terms_of_service.html)
* Data Encryption
    * [DM.4.01 - Encryption of Data in Transit](./guidance/DM.4.01_encryption_of_data_in_transit.html)
    * [DM.4.02 - Encryption of Data at Rest](./guidance/DM.4.02_encryption_of_data_at_rest.html)
* Data Removal
    * [DM.7.03 - Data Retention and Disposal Policy](./guidance/DM.7.03_data_retention_and_disposal_policy.html)

## Identity and Access Management
* Logical Access Account Lifecycle
    * [IAM.1.01 - Logical Access Provisioning](./guidance/IAM.1.01_logical_access_provisioning.html)
    * [IAM.1.02 - Logical Access De-provisioning](./guidance/IAM.1.02_logical_access_deprovisioning.html)
    * [IAM.1.04 - Logical Access Review](./guidance/IAM.1.04_logical_access_review.html)
    * [IAM.1.05 - Transfers: Access De-provisioning](./guidance/IAM.1.05_role_change_access_deprovisioning.html)
    * [IAM.1.06 - Shared Logical Accounts](./guidance/IAM.1.06_shared_logical_accounts.html)
    * [IAM.1.07 - Shared Account Restrictions](./guidance/IAM.1.07_shared_account_restrictions.html)
    * [IAM.1.08 - New Access Provisioning](./guidance/IAM.1.08_new_access_provisioning.html)
    * [IAM.1.09 - Access Modification](./guidance/IAM.1.09_access_modification.html)
* Authentication
    * [IAM.2.01 - Unique Identifiers](./guidance/IAM.2.01_unique_identifiers.html)
    * [IAM.2.02 - Password Authentication](./guidance/IAM.2.02_password_authentication.html)
    * [IAM.2.03 - Multifactor Authentication](./guidance/IAM.2.03_multifactor_authentication.html)
    * [IAM.2.04 - Authentication Credential Maintenance](./guidance/IAM.2.04_authentication_credential_maintenance.html)
    * [IAM.2.08 - Account Lockout](./guidance/IAM.2.08_account_lockout.html)
* Role-Based Logical Access
    * [IAM.3.02 - Source Code Security](./guidance/IAM.3.02_source_code_security.html)
    * [IAM.3.03 - Service Account Restrictions](./guidance/IAM.3.03_service_account_restriction.html)
    * [IAM.3.05 - Administrator Access to Production](./guidance/IAM.3.05_administrator_access_production.html)
* Remote Access
    * [IAM.4.01 - Remote Connections](./guidance/IAM.4.01_remote_connections.html)
    * [IAM.4.03 - Remote Maintenance: Authentication Sessions](./guidance/IAM.4.03_remote_maintenance_authentication_sessions.html)
* Key Management
    * [IAM.6.01 - Key Repository Access](./guidance/IAM.6.01_key_repository_access.html)
    
## Incident Response
* Incident Response
    * [IR.1.01 - Incident Response Plan](./guidance/IR.1.01_incident_response_plan.html)
    * [IR.1.03 - Incident Response](./guidance/IR.1.03_incident_response.html)
    * [IR.1.04 - Insurance Policy](./guidance/IR.1.04_insurance_policy.html)
* Incident Communication
    * [IR.2.01 - External Communication of Incidents](./guidance/IR.2.01_external_communication_of_incidents.html)
    * [IR.2.02 - Incident Reporting Contact Information](./guidance/IR.2.02_incident_reporting_contact_information.html)
    * [IR.2.03 - Incident External Communication](./guidance/IR.2.03_incident_external_communication.html)

## Network Operations
* Perimeter Security
    * [NO.1.01 - Network Policy Enforcement Points](./guidance/NO.1.01_network_policy_enforcement_points.html)
* Network Segmentation
    * [NO.2.01 - Network Segmentation](./guidance/NO.2.01_network_segmentation.html)
    
## People Resources
* On-boarding
    * [PR.1.01 - Background Checks](./guidance/PR.1.01_background_checks.html)
    * [PR.1.02 - Performance Management](./guidance/PR.1.02_performance_management.html)
    * [PR.1.03 - Policy and Procedure Review](./guidance/PR.1.03_policy_procedure_review.html)
    * [PR.1.04 - Hiring Review by Management](./guidance/PR.1.04_hiring_review_management.html)
    * [PR.1.05 - Job Descriptions](./guidance/PR.1.05_job_descriptions.html)

## Risk Management
* Risk Assessment
    * [RM.1.01 - Risk Assessment](./guidance/RM.1.01_risk_assessment.html)
    * [RM.1.02 - Continuous Monitoring](./guidance/RM.1.02_continuous_monitoring.html)
    * [RM.1.04 - Service Risk Rating Assignment](./guidance/RM.1.04_service_risk_rating_assignment.html)
    * [RM.1.05 - Risk Management](./guidance/RM.1.05_risk_management.html)
* Controls Implementation
    * [RM.3.01 - Remediation Tracking](./guidance/RM.3.01_remediation_tracking.html)

## Security Governance
* Policy Governance
    * [SG.1.01 - Policy and Standard Review](./guidance/SG.1.01_policy_and_standard_review.html)
* Security Documentation
    * [SG.2.01 - Information Security Program Content](./guidance/SG.2.01_information_security_program_content.html)
* Information Security Management System
    * [SG.5.03 - Security Roles and Responsibilities](./guidance/SG.5.03_security_roles_and_responsibilities.html)
    * [SG.5.06 - Board of Director Bylaws](./guidance/SG.5.06_bod_bylaws.html)
    * [SG.5.07 - Board of Directors Security Program Content](./guidance/SG.5.07_bod_security_program_content.html)

## Service Lifecycle
* Release Management
    * [SLC.1.01 - Service Lifecycle Workflow](./guidance/SLC.1.01_service_lifecycle_workflow.html)
    * [SLC.1.03 - Release Notes](./guidance/SLC.1.03_release_notes.html)
* Source Code Management
    * [SLC.2.01 - Source Code Management](./guidance/SLC.2.01_source_code_management.html)

## Systems Design Documentation
* Internal System Documentation
    * [SDM.1.01 - System Documentation](./guidance/SDM.1.01_system_documentation.html)
    
## Systems Monitoring
* Logging
    * [SYS.1.01 - Audit Logging](./guidance/SYS.1.01_audit_logging.html)
    * [SYS.1.02 - Secure Audit Logging](./guidance/SYS.1.02_secure_audit_logging.html)
    * [SYS.1.07 - Audit Log Capacity and Retention](./guidance/SYS.1.07_audit_log_capacity_retention.html)
* Security Monitoring
    * [SYS.2.01 - Security Monitoring Alert Criteria](./guidance/SYS.2.01_security_monitoring_alert_criteria.html)
    * [SYS.2.07 - System Security Monitoring](./guidance/SYS.2.07_system_security_monitoring.html)

## Third Party Management
* Vendor Assessments
    * [TPM.1.01 - Third Party Assurance Review](./guidance/TPM.1.01_third_party_assurance_review.html)
    * [TPM.1.02 - Vendor Risk Management](./guidance/TPM.1.02_vendor_risk_management.html)
    * [TPM.1.04 - Vendor Compliance Monitoring](./guidance/TPM.1.04_vendor_compliance_monitoring.html)
* Vendor Agreements
    * [TPM.2.02 - Vendor Non- disclosure Agreements](./guidance/TPM.2.02_vendor_non-disclosure_agreements.html)
    * [TPM.2.05 - Customer and Vendor Commitments and Responsibilities Documentation](./guidance/TPM.2.05_customer_vendor_commitments_responsibilities_documentation.html)
    * [TPM.2.06 - Mutual Non-Disclosure Agreements](./guidance/TPM.2.06_mutual_non_disclosure_agreements.html)
* Vendor Procurement
    * [TPM.3.01 - Approved Service Provider Listing](./guidance/TPM.3.01_approved_service_provider_listing.html)

## Training and Awareness
* General Awareness Training
    * [TRN.1.01 - General Security Awareness Training](./guidance/TRN.1.01_general_security_awareness_training.html)
    * [TRN.1.02 - Code of Conduct Training](./guidance/TRN.1.02_code_of_conduct_training.html)
* Role-Based Training
    * [TRN.2.01 - Developer Security Training](./guidance/TRN.2.01_developer_security_training.html)

## Vulnerability Management
* Production Scanning
    * [VUL.1.01 - Vulnerability Scans](./guidance/VUL.1.01_vulnerability_scans.html)
    * [VUL.1.03 - Approved Scanning Vendor](./guidance/VUL.1.03_approved_scanning_vendor.html)
* Penetration Testing
    * [VUL.2.01 - Application & Infrastructure Penetration Testing](./guidance/VUL.2.01_application_and_infrastructure_penetration_testing.html)
* Patch Management
    * [VUL.3.01 - Infrastructure Patch Management](./guidance/VUL.3.01_infrastructure_patch_management.html)
    * [VUL.3.02 - End of Life Software](./guidance/VUL.3.02_end_life_software.html)
* Malware Protection
    * [VUL.4.01 - Enterprise Protection](./guidance/VUL.4.01_enterprise_protection.html)
* Code Security
    * [VUL.5.01 - Code Security Check](./guidance/VUL.5.01_code_security_check.html)
* External Advisories and Inquiries
    * [VUL.6.01 - External Information Security inquiries](./guidance/VUL.6.01_external_information_security_inquiries.html)
