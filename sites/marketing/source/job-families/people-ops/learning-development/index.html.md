---
layout: job_family_page
title: "Learning & Development Roles"
description: "The Learning and Development (L&D) job family is responsible for building and maintaining a learning culture at GitLab."
---

The Learning and Development (L&D) job family is responsible for building and maintaining a learning culture at GitLab. Specifically, L&D plays a strategic role in the organization to ensure team members acquire and build skills, knowledge, and competencies that will enable them to perform at a high level. The goal of the job family is to develop and change the behavior of team members and groups to reach organizational objectives and results. 

## Responsibilities 

- Solicit and incorporate team member feedback into our course content and experience
- Enhance and build out our Handbook resources for learning
- Works to develop a comprehensive GitLab learning curriculum that incorporates multi-modality learning formats (bite-sized training, virtual instructor-led, hands-on workshops, self-study, etc) while ensuring all material is Handbook first
- Supports the marketing strategy and develops communication material to promote internal training
- Work [handbook first](/handbook/handbook-usage/#why-handbook-first)

## Requirements

- Ability to use GitLab
- Strong interpersonal and communication skills
- Comfortable delivering training to various functional groups and audience sizes
- Ability to bring innovative Learning & Development ideas and proposals to the team that can improve team member career development
- Self-starter, ability to learn fast and contribute
- Strong organizational knowledge and understanding of the current gaps within career development and L&D at GitLab
- Passion for career development, training, leadership, and helping team members develop to their fullest potential
- The ability to thrive in a fast-paced environment
- Team-orientated, engaging, and energetic
- Share our [values](/handbook/values/), and work in accordance with those values

## Levels

### Learning & Development Associate 

The Learning & Development Associate reports to the [Learning & Development Partner](#learning-development-partner).

#### Learning & Development Associate Job Grade

The Learning & Development Associate is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

#### Learning & Development Associate Responsibilities

- Support the identification and design of learning solutions to support GitLab's growth
- Maintains the back-end management of learning administration to include certifications, knowledge assessments, external learning inquires, and internal training programs
- Develops Learning Management System (LMS) adoption guides for team members and enablement activities with guidance from the L&D Partner 
- Supports the L&D Generalist with integrating the LMS into GitLab. Utilizes the LMS course authoring tool to create Handbook first course content
- Develop customized learning paths for team members that incorporate customized and curated learning content
- Track and monitor training consumptions through reporting and analytics

#### Learning & Development Associate Requirements

- Interest in developing skills in adult learning theory and instructional design while applying those lessons to new learning content at GitLab
- Nice to have: experience with instructional design and developing learning content
- Passion for supporting team members growth and development while helping to build a learning culture at GitLab 
- 0-2 years of work experience in the areas of training delivery, instructional design, or learning technology

#### Learning & Development Associate Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/). 

* 30-minute interview with L&D Partner 
* 30-minute interview with L&D Generalist
* 30-minute interview with a People Business Partner (PBP)
* A final assessment, candidates may be invited to give a short 10-minute presentation on managing underperformance to the L&D team

### Learning & Development Generalist

The Learning & Development Generalist reports to the [Learning and Development Partner](#learning-development-partner).

#### Learning & Development Generalist Job Grade

The Learning & Development Generalist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Learning & Development Generalist Responsibilities

- Extends the Learning & Development Associate responsibilities
- Support the L&D Partner to identify, develop, and deliver programs that support the growth of our team members and GitLab
- Work with the L&D Partner to roll out an internal learning and development roadmap for all GitLab managers and individual contributors
- Research, develop, and facilitate exciting and impactful asynchronous and synchronous L&D programs
- Contribute to the course catalog which will include e-learning, instructor-led courses, and hands-on workshops, utilizing the GitLab handbook to ensure the trainings are accessible to everyone
- Own the L&D metrics and provide feedback to the L&D Partner to iterate upon programs and courses for continuous improvement

#### Learning & Development Generalist Requirements

- Extends the Learning & Development Associate requirements
- 2-3 years of work experience in the areas of training delivery, instructional design, or learning technology

### Learning & Development Partner

The Learning & Development Manager reports to the [Senior Director, People Success](job-families/people-ops/people-leadership/#senior-director-people-success).

#### Learning & Development Partner Job Grade

The Learning & Development Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Learning & Development Partner Responsibilities

- Extends the Learning & Development Generalist responsibilities
- Partner with members of the People group and the leadership team to identify, develop, and deliver solutions that support the growth of our team members and GitLab's success
- Establish an internal learning and development roadmap for all GitLab managers and individual contributors
- Iterate on existing materials and design and develop new L&D content, utilizing [GitLab's YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) and [handbook](/handbook/handbook-usage/)
- Collaborate with the Sales team to design and deliver orientation content that develops new hires' understanding of GitLab’s business and platform
- Select a learning platform for GitLab that incorporates content from the handbook and provides methods for tracking assessments and completion
- Create the course catalog
- Monitor L&D metrics and iterate upon programs and courses for continuous improvement
- Create and design the supporting course material for all development programs, both for instructor-led and e-learning
- Identify and design the right learning solutions to support GitLab's growth
- Build and scale initiatives that focuses on company culture, individual development, strengthening our leadership, and organizational learning

#### Learning & Development Partner Requirements

- Extends the Learning & Development Generalist requirements
- 5+ years experience in related work such as instructional design and developing learning content
- Track record of designing engaging and impactful development programs that improves individual, team, and company performance
- Exceptional written and interpersonal skills
- Experience developing self-service learning content, such as e-learning modules
- Experience designing and delivering webinars or synchronous online courses
- Strong logistical planning and organizational skills
- Capable of working collaboratively across multiple departments
- Passionate about personal development, training, learning, and seeing individuals develop to their fullest potential
- Deep experience in assessing organizational needs and developing a variety of learning solutions to drive development and growth within the company

#### Learning & Development Partner Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Director of People Operations.
* Next, the candidate will be invited to interview with a member of the L&D team, a member from our Sales Enablement team, a People Business Partner and our Internal Strategy Consultant.
* After that, our CEO may choose to conduct a final interview.
* As a final assessment, candidates will be required to prepare and present a short 15 minute training on giving feedback, to the Director of People Operations.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### Learning & Development Manager 

The Learning & Development Manager reports to the [Senior Director, People Success](job-families/people-ops/people-leadership/#senior-director-people-success).

#### Learning & Development Manager Job Grade

The Learning & Development Partner is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Learning & Development Manager Responsibilities

- Extends the Learning & Development Partner responsibilities
- Develop and lead the Learning and Development team while creating quality learning lessons and programs
- Design and develop role-based learning paths for individual contributors, managers, aspiring managers, and executive leadership 
- Advise People group and the leadership with organization wide training initiatives that support the growth of our team members and GitLab’s success
- Design, develop, and deploy a comprehensive eLearning, microlearning, and live learning curriculum for all team members with role-based learning paths for managers, team members, aspiring managers, and executives
- Integrate the Learning Management System into GitLab and manage the change management and adoption activities and ensure all content is Handbook first
- Introduce new learning vehicles that reinforce a personalized learning infrastructure that includes curated content, customized content, and eLearning modules
- Train and coach managers and executive leadership and others involved in employee development efforts
- Develop and introduce training development plans that complement performance development and enablement plans 
- Apply adult learning theory best practices and the latest learning trends into GitLab that are handbook first
- Conduct annual learning needs analysis that identify functional and organization wide training goals 
- Review future workforce competency requirements and learning needs to identify gaps in current learning programs and map to proposed curriculum
- Define learning priorities and governance of learning programs by introducing open enrollment courses where team members can self-select courses of interest
- Identify current and future workforce requirements by working with GitLab leaders to understand what the workforce will need to do in the future to deliver on business objectives
- Efficiently manage learning and development budgets 

#### Learning & Development Manager Requirements

- Extends the Learning & Development Partner requirements
- 7+ years of work experience in the areas of training delivery, instructional design, learning technology, and people management 
- Strong project management, leadership, change management, and cross-functional collaboration skills with a track record of managing complex learning projects
- Ability to seamlessly bridge across technical and non-technical groups to achieve an aligned outcome
- Self-organized with the ability to think strategically about learning design, business needs, strategic priorities, employee development, and employee engagement
- Robust experience delivering presentations to diverse audiences with a range of backgrounds, social styles, learner profiles, and job duties
- Experience collaborating across multiple stakeholder groups to deliver global solutions
- Demonstrated analytical problem solving with an ability to manage through ambiguity
- Experience designing and delivering self-service learning content and building a learning infrastructure 
- Comprehensive knowledge and proven experience with learner-centered, performance based instructional theories and adult learning principles
- Demonstrated excellence in written and verbal communication, organizational skills, and project management
- Experience in influencing senior leaders/stakeholders at the director and above levels

#### Learning & Development Manager Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Director of People Success.
* Next, the candidate will be invited to interview with a member of the L&D team, a member from our Sales Enablement team, a People Business Partner and our People Operations team.
* Next, the candidate will interview with our Chief People Officer 
* After that, our CEO may choose to conduct a final interview.
* As a final assessment, candidates will be required to prepare and present a short 15 minute training on giving feedback, to the Senior Director of People Success

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Performance Indicators
- [Engagement Survey Growth and Development Score](/handbook/people-group/learning-and-development/#engagement-survey-growth-and-development-score--x)
- [Rate of internal job promotions](/handbook/people-group/learning-and-development/#rate-of-internal-job-promotions--x)
- [12 month voluntary team member turnover related to growth](/handbook/people-group/learning-and-development/#12-month-voluntary-team-member-turnover-related-to-growth--x)

## Learning & Development Career Ladder

The next step in the Learning and Development job family is to move into a senior manager role which is not yet defined at GitLab. 
