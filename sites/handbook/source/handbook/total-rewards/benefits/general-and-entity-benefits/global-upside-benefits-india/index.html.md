---
layout: handbook-page-toc
title: "Global Upside Benefits"
description: "Global Upside Benefits specific to India based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## India 

All of the benefits listed below are administered and managed by [Global Upside](https://globalupside.com/). As part of the onboarding process Global Upside will reach out to team members in their first week to arrange setup and enrollment.  Should you have any questions, please contact:

| Email | Purpose |
| ------ | ------ |
| `hr@globalpeoservices.com` | Employment-related questions |
| `benefitsops@globalupside.com` | Questions regarding benefits elections |

### Medical Benefits

* Group Mediclaim Policy is arranged through [ICICI Lombard](https://www.icicilombard.com/)
* Group Mediclaim Policy which will take care of hospitalization expenses, the coverage provided is available for the team member, their spouse and up to two children.  Should additional cover be required this will need to be purchased by the team member in the form of an Individual Policy. This can not be purchased under the Group Mediclaim Policy.
* Group Personal Accident policy including accidental death benefit.
* For additional information on how [Benefits](https://drive.google.com/file/d/1jd1gCLiWZTwIeaDgnvo8SX1TXDNbYnrW/view?usp=sharing) operate, please check out the documentation on the Google Drive.

### Pension

Global Upside has a provident fund that the members pay to the government. This is included in the CTC. It ensures retirement benefits and family pensions on death in service. EPF benefits typically extend to all employees in an organization

* Member Contribution(% of base salary):
EPF: 3.67% and EPS: 8.33%, Total Contribution: 12%

* Employer Contribution(% of base salary):
EPF: 12%, EPS: None
Salary Ceiling: EPF: Mandatory contribution remains up to a monthly salary ceiling of INR15,000

### Life Insurance

Most companies in India do not offer life insurance as part of the benefits package. Global Upside, similarly, does not offer a life insurance plan to GitLab team members. Most workers in India will typically get their own life insurance which can be portable throughout their lifetime.

### Meal Vouchers

Sodexo Meal Cards are an optional benefit. These Meal Cards work like a Debit Card, which can be used at any outlet selling food items and non-alcoholic beverages that accept card payments. If you would like purchase these a deduction from salary will be made each month. All team members are gien the option to opt-in for Sodexo cards during the pre-onboarding process managed by the CES (Candidate Experience Specialist) team.  

### Global Upside India Parental Leave 

#### Statutory Leave Entitlement

**Maternity Leave:** Team members can take up to 26 weeks of Maternity Leave (for up to 2 occassions).

**Paternity Leave:** Team members can take up to 3 days of Paternity Leave.

#### Maternity Leave Payment
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 26 weeks.
* Every female team member availing maternity leave is eligible for an insurance coverage of INR 50,000 for expenses related to delivery.

#### Paternity Leave Payment
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.

#### Applying for Parental Leave in India
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

### Gratuity

Gratuity is a statutory offering in India per The Payment of Gratuity Act, 1972 which is offered through Global Upside based on the Global Upside hire date. 

### Loyalty Bonus Scheme

GitLab has implemented a private loyalty bonus scheme since some team members have been contracted through different employment types as we have worked through country conversion processes. Therefore, the GitLab Loyalty Bonus Scheme will supplement gratuity based on the GitLab hire date vs the PEO hire date. 

**Elibility:** One can only claim Loyalty Bonus after retirement or resign from the service after completing 5 years of continuous service.

**Forfeiture of Loyalty Bonus:** The full amount of Loyalty Bonus can be forfeited if a team member’s services have been terminated due to: a) Riotous or disorderly conduct or any other violent act; b) Committing an offence involving moral turpitude.

**Calculation of Loyalty Bonus:**

Loyalty Bonus Amount =  Gratuity Calculation using GitLab Hire Date + Gratuity Calculation using PEO Hire Date

`The duration for Gratuity Calculation using GitLab Hire Date is the duration prior to the hire date of the PEO`

**Note:** The Loyalty Bonus Amount is provided by GitLab to the team members. Any amount which GitLab will pay as Loyalty Bonus would be fully taxable for the team members. 

**Total Rewards Responsibilities:**
  * Maintain Gratuity Calculation sheet
  * Update the sheet every month
  * Send an email to Accounts team on or before 5th of every month to calculate the accruals.

## Philippines

All of the benefits listed below are administered and managed by [Global Upside](https://globalupside.com/).

### Social Security System

The Philippine Social Security System has employer and team member contributions to cover benefits such as pension, unemployment, maternity disability, sickness, death and funeral. 

### Home Development Mutual Fund (HDMF)

The HDMF, also known as the Pag-IBIG Fund, is a provident savings system to provide affordable shelter financing to members employed by local / foreign-based employers as well as self-employed members.

### Philippine Health Insurance Corporation (PhilHealth)

PhilHealth is a government corporation attached to the Department of Health. It provides health insurance coverage and ensures affordable & accessible health care services for all members.

### Medical Benefits

* GitLab offers International Healthcare Insurance through [NOW Health International](https://www.now-health.com/en/) which covers 100% of team member contributions and 66% for spouse & kids of premiums for medical, vision, dental and maternity coverage.
* For specific information on how your benefits operate, please read the [NOW WorldCare Member's Handbook](https://drive.google.com/drive/search?q=%22PH-NOW%20WorldCare%20Members%20Handbook%22). Feel free to also peruse NOW Health International’s [document library](https://www.now-health.com/en/document-library/).

### 13th Month Pay

The 13th month pay is mandatory in the Philippines under the labor code and by Presidential Decree 851. 
Every level of team member is entitled to 13th month pay as long as they have worked at least one month during the calendar year. It is calculated as 1/12 of the total basic salary earned during the year. It has to be paid out by December 24 or the end of the contract, whichever is sooner.

### Vacation Leave

Team members are entitled to `five days` of paid vacation leave after 12 months of service, referred to as "service incentive leave" by the Labour Code.

### Global Upside Philippines Parental Leave

#### Statutory Leave Entitlement

**Maternity Leave:** Team members can take up to 105 days of Maternity Leave. Team members may request an additional 30 days of unpaid Maternity Leave. Solo mothers have the option to extend by 15 days. 

**Paternity Leave:** Team members can take up 7 days of Paternity Leave or up to 14 days if the team member's wife allocates him 7 days of extra Paternity Leave.

#### Maternity Leave Payment
* Team members may receive Maternity Leave payment from the SSS for up to 105 days. 
* If [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement your pay to ensure you receive 100% pay for up to 16 weeks of your leave.

#### Paternity Leave Payment
* Team members may receive Paternity Leave payment from the SSS for up to 7 days (if allocated from the wife's Maternity Leave payment).
* If [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement your pay to ensure you receive 100% pay for up to 16 weeks of your leave.

#### Applying for Parental Leave in the Philippines
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).
