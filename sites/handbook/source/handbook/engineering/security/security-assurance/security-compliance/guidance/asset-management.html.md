---
layout: handbook-page-toc
title: "Asset Management Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose
GitLab maintains an inventory of systems and technology assets from purchase through disposition, to ensure secured use, regardless of the asset's location.

## Scope
This control applies to all GitLab endpoint workstations as well as virtual assets within our hosting providers. 

## Ownership
* IT Operations owns the workstation assets portion of this control
* Infrastructure owns the system and service portions of this control

## Controls

| Control | Description | Goal | TOD | TOE | 
|:---------|:-------------|:------|:-----|:-----|
| AST-09 | Secure Disposal or Re-Use of Equipment | Does the organization securely destroy media when it is no longer needed for business or legal reasons? | 1. Inspect formal policies, procedures or other relevant documentation that outline mechanisms used to securely destroy media when no longer needed for business or legal purposes. * 2. | 1. Examine data destruction policies, procedures and configurations for evidence that the procedures, policies and configurations facilitate implementation and adherence of media destruction when no longer needed for business or legal purposes. |

* *Test of Design* - (TOD) – verifies that a control is designed appropriately and that it will prevent or detect a particular risk. 
* *Test of Operating Effectiveness* - (TOE) - used for verifying that the control is in place and it operates as it was designed.

### Policy Reference
*  [Endpoint Management at GitLab](/handbook/business-ops/team-member-enablement/onboarding-access-requests/endpoint-management/)
