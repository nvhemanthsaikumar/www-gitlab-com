---
layout: handbook-page-toc
title: "Change Management Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose
GitLab governs changes in a sustainable and ongoing manner that involves active participation from both technology and business stakeholders to ensure that only authorized changes occur.

## Scope
This control applies to all changes on all systems within our production environment that support the business of GitLab.com.

## Ownership
The owner of this control is the Infrastructure Team. <br>
The Process owner(s) are Infrastructure, IT Ops and Security.

## Controls
| Control | Description | Goal | TOD | TOE | 
|:---------|:-------------|:------|:-----|:-----|
| CHG-01 | Change Management Program | Does the organization facilitate the implementation of change management controls? | *1. Identify policies and procedures responsible for the implementation of change management security controls. *2. Examine policies and procedures for: purpose; scope; roles and responsibilities; management commitment; coordination among organizational entities; compliance; and implementation requirements. | *1. Examine change control policies, procedures and user-access security controls to ensure coverage and appropriate configuration of applicable change management processes. *2. Inspect a sample of controlled documents to evidence they are reviewed and approved in accordance to TOD. *3. Pull a population of change control records. *4. Inspect a sample of controlled documents to evidence that change management mechanisms exist to identify and document in accordance to TOD. | 
| CHG-02 | Configuration Change Control | Does the organization govern the technical configuration change control processes? | *1. Inquire of appropriate personnel to determine the process for making changes to production. *2. Inspect the Change Management policy to determine the process for making changes to production. | *1. Obtain and inspect a system generated listing of all changes (commits) to the system during the period under review. *2. Select an annualized sample of changes that occurred during the period to determine if they were tested and approved in line with the change management policy by all relevant business and IT stakeholders. *3. Obtain and inspect documentation (i.e merge requests/issues) that each sampled change was tested in line with the Change Management policy by all relevant business and IT stakeholders. *4. Obtain and inspect documentation (i.e merge requests/issues) that each sampled change was approved in line with the Change Management policy by all relevant business and IT stakeholders. *5. Obtain and inspect documentation that each sampled change that was migrated to production was the same change that was approved for migration. | 
| CHG-03 | Security Impact Analysis for Changes | Does the organization analyze proposed changes for potential security impacts, prior to the implementation of the change? | *1. Identify policies and procedures that outline security and business impact assessment requirements prior to initiating a change. | *1. Pull a population of proposed changes during the examination period Confirm potential security and business impacts were identified, assessed and approved prior to initiating the change. | 
| CHG-04 | Access Restriction For Change | Does the organization enforce configuration restrictions in an effort to restrict the ability of users to conduct unauthorized changes? | *1. Inquire of appropriate personnel to determine the process for migrating changes to production and what privileges/roles allow a user to migrate to production. *2. Inspect (user/role/privilege listing, user guide, other evidence) to determine which privileges/roles grant the user the ability to migrate and make changes to production. | *1. Obtain and inspect a listing of all accounts (user/system/service) for the system and their associated roles/privileges. Filter the listing for those roles/privileges with the ability to migrate and to make changes to production. *2. Obtain and inspect a listing of all current team members and their associated job title/roles. *3. Obtain and inspect a listing of all new hired team members during the period under audit. *4. Obtain and inspect a listing of all terminated team members. *5. For 100% of the accounts with the ability to migrate and make changes to production, determine the owner and their role/job title/account purpose. *6. For 100% of the accounts with the ability to migrate and make changes to production, determine if the account is owned by a terminated user. *7. For 100% of the accounts with the ability to migrate and make changes to production, determine if the account is owned by a user provisioned the access during the period under audit. If so, obtain evidence that the user was approved for the access prior to granting the access. *8. For 100% of the accounts with the ability to migrate and make changes to production, determine whether the account is owned by a non-developer. *9. For 100% of the accounts with the ability to migrate and make changes to production, determine whether the account is owned by an appropriate IT personnel. | 
| CHG-05 | Stakeholder Notification of Changes | Does the organization ensure stakeholders are made aware of and understand the impact of proposed changes? | *1. Identify policies, procedures and related documents that outline communication requirements to stakeholders outlining the impact and approval of proposed changes prior to initiating a change. | *1. Pull a population of proposed changes during the examination period Confirm communication and approval requirements outlined in the ToD step were applied to each change selected. | 
| CHG-06 | Security Functionality Verification | Does the organization verify the functionality of security controls when anomalies are discovered? | *1. Identify policies and procedures that verify the functionality of security controls when anomalies are discovered. *2. Interview key organizational personnel within GitLab to discuss high level workflows that support the discovery of anomalies. | *1. Identify detection and monitoring procedures or standards used to assist with the identification of anomalies. *2. Examine change detection mechanisms including but not limited to: system startup, restart, shutdown, and abort. *3. Examine notification mechanisms including electronic alerts to system administrators, messages to local computer consoles, and/or hardware indications such as lights. | 

* *Test of Design* - (TOD) – verifies that a control is designed appropriately and that it will prevent or detect a particular risk.
* *Test of Operating Effectiveness* - (TOE) - used for verifying that the control is in place and it operates as it was designed.

### Policy Reference
* [Change Management](/handbook/engineering/infrastructure/change-management/)
* [Service Lifecycle Workflow](/handbook/engineering/security/security-assurance/security-compliance/guidance/SLC.1.01_service_lifecycle_workflow.html)
* [Business Technology Change Management Workflow](/handbook/business-ops/business-technology-change-management/)
