---
layout: job_family_page
title: "Digital Marketing Programs Manager"
---

## Intro

You might describe yourself as analytical, creative, organized, and diplomatic. You have experience driving web traffic to build revenue by deploying a variety of crafts that span marketing, content, and product management. You enjoy setting up conversion rate optimization experiments to continually improve the results of a prospective customers journey, and sharing results and insights with a broad group of stakeholders throughout the company. You also enjoy employing a number of marketing tactics to attract a relevant audience to a marketing site.

## Responsibilities

- Ensure all online marketing enhances our brand with developers, IT ops practitioners, and IT leaders. Be an expert on our audiences and their preferences.
- Be an expert on our customer lifecycle, and ways in which customer needs are evolving/how our product meets them.
- Work with product team to implement marketing strategies that span into trial/product experience, and impact both customer conversion and future retention/upsell.
- Drive innovation and experimentation on the marketing site & trial experience to improve demand creation & conversion.
- Oversee our paid search and paid social programs, maximizing for return on marketing spend.
- Analyze and report data across multiple channels to monitor marketing site growth.

## Specialties and levels

### Digital Marketing
The ideal digital marketer for GitLab will understand the larger opportunity across marketing channels and will have experience in a wide range of paid marketing tactics as well as a solid background in SEO and the ability to make decisions about the best marketing channels to reach our audience.  They will also be able to use data and research to make marketing decisions and run conversion rate optimization experiments.

#### Digital Marketing Program Manager

##### Job Grade 

The Digital Marketing Program Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Responsibilities
- Assist with building and improving paid search and social campaigns.
- Assist in growing traffic to about.gitlab.com through paid search and social marketing programs.
- Work with marketing and sales ops to evaluate and implement adtech and martech.
- Work with our content team to ensure the content we create on the marketing site is promoted in paid and social marketing sites and sponsorships.
- Monitor market trends and competitors to determine marketing opportunities and recommend and take actions to improve visibility of site.
- Grow trial conversion to paying customer by optimizing the customer journey
- Develop and execute on strategies to improve free user to paid plan conversion.

##### Requirements
- 3+ years in Digital Marketing role
- 2+ years experience Google Analytics (or related tool) and Google Adwords and expert-level SEO/SEM tool experience.
- 1-3 years of enterprise software marketing experience.
- 1-3 years paid social media marketing experience
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Tableau, SQL, and/or similar.

####  Senior Digital Marketing Program Manager

##### Job Grade 

The Senior Digital Marketing Program Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Responsibilities
- Define our overall digital strategy together with marketing team and digital marketing agency.
- Manage our paid sponsorship opportunity and budget.
- Develop and manage our paid social and search opportunity and budget.
- Improve closed-loop reporting and analysis capabilities for paid search, social, sponsorships, ABM, and SEO.
- Drive innovation and experimentation on the marketing site & trial experience to improve inbound demand creation & conversion.

##### Requirements
- 5+ years in Digital Marketing role
- 5+ years experience Google Analytics (or related tool) and Google Adwords and expert-level SEO/SEM tool experience.
- 3+ years of enterprise software marketing experience.
- 3+ years paid social media marketing experience
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Tableau, SQL, and/or similar.

#### Manager, Digital Marketing Programs

The Manager of Digital Marketing Programs for GitLab should have a background and hands-on experience in all areas of marketing: SEO, SEM, Paid and Organic Social, ABM, Paid sponsorships, and Conversion Rate Optimization. They will have experience managing marketing teams and working with marketing agencies. They should also be able to maintain a budget, use analytics tools, CRM, MAT, and other marketing tools, and have a data-driven approach to marketing. Experience in content marketing and website maintenance are also helpful in this role.

##### Job Grade 

The Manager, Digital Marketing Programs is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Responsibilities
- Administer the Digital Marketing Programs marketing budget and ensure marketing costs are tracked correctly.
- Manage, build, and lead a strong team by coaching and developing existing members and closing talent gaps where needed through acquisition of new team members.
- Build and implement Digital Marketing Programs strategy—understand key partnerships within organization to drive full funnel of customer acquisition, retention, and upsell.
- Define, manage, and implement our overall digital strategy together with marketing team and digital marketing agency.
- Create strategies for conversion rate optimization (CRO) projects.
- Lead effort to create standardized marketing reporting and dashboards, presenting performance to the marketing department as well as stakeholders throughout the company.
- Oversee our paid search, social, sponsorships, and account based advertising programs, maximizing for return on marketing spend.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

##### Requirements
- 10+ years in a Digital Marketing role
- 5+ years of enterprise software marketing experience.
- Experience managing a marketing team.
- In-depth industry experience and knowledge in at least one digital marketing or marketing programs specialty.
- Strong technical marketing and advanced digital analytics skills

## Career Ladder

The next step in the Digital Marketing Programs job family is not yet defined at GitLab. 

## Performance Indicator

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
* Selected candidates will be invited to schedule an interview with the Senior Director, Revenue Marketing.
* Candidates will then be invited to schedule two separate interviews with members of the Digital Marketing Programs team.
* Finally, candidates may be asked to interview with our CMO
* Successful candidates will subsequently be made an offer via video or phone call
