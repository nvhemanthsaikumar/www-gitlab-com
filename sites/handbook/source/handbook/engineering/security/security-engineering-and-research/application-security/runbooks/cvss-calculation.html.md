---
layout: handbook-page-toc
title: "CVSS Calculation"
---

# CVSS Calculation

Here are some CVSS calculation guidelines to make sure we are consistent across 
different team members. You should consider the vulnerabiliy as it affects self-hosted installs of GitLab.

Accuracy and consistency of these scores is important, at any time when deciding
on a score do not hesitate to ask for advice in the Bug Bounty Council issue or by
reaching out to team members by any way you see fit.

This runbook is based on the [FIRST CVSS user guide](https://www.first.org/cvss/user-guide),
especially the [Scoring Rubrics section](https://www.first.org/cvss/user-guide#Scoring-Rubrics).
To obtain the actual score you can use a CVSS calculator such as
[FIRST's](https://www.first.org/cvss/calculator/3.1) or [NVD's](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator)
while going through this runbook.

## Exploitability Metrics

### Attack Vector (`AV`)

- **Network** (`AV:N`) for attacks that can be done remotely, this is the case for nearly all of our security issues
- **Adjacent network** (`AV:A`) for attacks where the attacker needs to be on the same local network
- **Local** (`AV:L`) for attacks that require to be logged in the machine or that the victim runs something locally
- **Physical** (`AV:P`) for attacks requiring physical access to the machine

### Attack Complexity (`AC`)

- **Low** (`AC:L`) if there is no condition that's out of the attacker's control that's required to carry out the attack
  - **Examples**
    - IDOR using simple guessable ID
    - Stored XSS on a page that's part of the user's normal workflow (main project page, issue or merge request page, etc.)
- **High** (`AC:H`) if a succesful attack depends on conditions beyond the attacker's control
  - **Examples**
    - Knowledge of a private project name is required to carry out the attack
    - A certain setting has to have a non-default value to make the attack possible
    - CSRF and reflected XSS because it's likely the victim will not click the attacker's link
    - Stored XSS on an obscure page that the victim's unlikely to visit without clicking a link from the attacker (on a specific job's build log for example)

### Privileges Required (`PR`)

- **None** (`PR:N`) if the attack can be carried by an unauthenticated user, as it pertains to self-hosted Gitlab instances, or by a user with a self-registered account on Gitlab.com, with no specific project membership needed (other than on projects owned by the attacker)
  - **Examples**
    - Permission issues allowing an unauthenticated account to access confidential information through the API
    - CSRF or reflected XSS issues, assuming a privileged account isn't required to craft the attack URL
- **Low** (`PR:L`) if the attack requires sub-Maintainer membership to a specific project
- **High** (`PR:H`) if the attack requires Maintainer or above membership to a specific project, or instance admin rights
  - Side note: high privilege users using a bug to sabotage their own projects is out of scope of our bug bounty program

### User Interaction (`UI`)

- **None** (`UI:N`) if the attack doesn't require any interaction from the user
  - **Examples**
    - Any attack that would work even if the victim never logs back in to GitLab
- **Required** (`UI:R`) otherwise
  - **Examples**
    - All vulnerabilities that need a victim to do any stort of action even if the action is only to log on GitLab, this includes all XSS and CSRF vulnerabilities

### Scope (`S`)

FIRST has this note: if a Scope change has not occurred, Confidentiality, Integrity and
Availability impacts reflect consequence to the vulnerable component, otherwise they reflect
consequence to the component that suffers the greater impact.

- **Unchanged** (`S:U`) if the vulnerable component is the same as the impacted component
- **Changed** (`S:C`) if they're different
  - **Examples**
    - XSS (vulnerable component is the website, impacted component is the browser)
    - SSRF in GitLab that allows fetching GCP metadata
    - [Customers's Portal bug that allows to get an admin account on GitLab](https://about.gitlab.com/blog/2019/11/29/shopping-for-an-admin-account/)

## Impact Metrics

### Confidentiality Impact (`C`)

- **None** (`AC:N`) when no confidential information is disclosed
- **Low** (`AC:L`) when access to some restricted information is obtained, but the attacker does not have control over what information is obtained, or the amount or kind of loss is limited
  - **Examples**
    - Access to private issue/MR titles but not their content
    - Access to a small number of private issues/MR (one or a handful of projects, as opposed to being able to read any private issue on the instance)
    - Access to private data that the attacker doesn't have access to anymore, but had access to in the past
    - Access to private data of minor importance (issue due dates, private project name, etc.)
- **High** (`AC:H`) when there's sensitive information disclosure such as access token, runner token, private repository and more

### Integrity Impact (`I`)

- **None** (`AC:N`) when there's no integrity loss
- **Low** (`AC:L`) when some information can be modified or the attacker does not have control over what information or the amount
  - **Examples**
    - Very similar to the examples for Confidentiality above, but assuming we can modify those values and not only see them
- **High** (`AC:H`) when the attacker can modify critical information or modify large numbers of items
  - **Examples**
    - Attacker can add a malicious Runner to a project where they don't have the required permissions to do so
    - Attacker can add a malicious OAuth application to the victim's trusted apps
    - Attacker can modify all issues on the GitLab instance

### Availability Impact (`A`)

- **None** (`AC:N`) when there's no availability impact
- **Low** (`AC:L`) when access is denied to a non-critical resource or only a part of the system is affected
  - Examples
    - A small amount of projects are inaccessible but become available when the attack stops
    - A small amount of users can't use the instance
- **High** (`AC:H`) when access is denied to a critical resource or the entire system is affected
  - **Examples**
    - The attacker can delete projects or groups
    - Runners all stop picking up pipelines
    - GitLab instance taken down
