[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host            |
| ---------- | --------------- |
| 2021-03-03 | Taurie          |
| 2021-03-17 | Mike            |
| 2021-03-31 | Jacki           |
| 2021-04-14 | Justin          |
| 2021-04-21 | APAC/Europe (Marcel) |
| 2021-04-28 | Marcel          |
| 2021-05-12 | TBD             |
| 2021-05-26 | Taurie          |
| 2021-06-09 | Mike            |
| 2021-06-23 | Jacki           |
