---
layout: markdown_page
title: "Continuous integration in Agile development"
description: "Learn more about how continuous integration and Agile methodologies work together."
---

Agile development is a collection of methodologies that focus on iterative development and collaboration between self-organized teams. Agile methods rely on disciplined project management processes and cooperation between teams, or even teams of teams.

![Team structure in Agile development](/images/blogimages/team-teams2.png){: .shadow.left.wrap-text}

While there are many Agile frameworks to choose from, all Agile development projects involve some sort of continuous development, such as continuous planning, continuous testing, and continuous integration (CI). 

With CI, developers gradually grow a stable system by working in small batches and short cycles, which is a lean concept. Continuous integration supports teams working within Agile frameworks with CI/CD pipelines that automate builds, tests, and deployments. While Agile falls into [Project and Portfolio Management](/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/){:target="_blank"} (PMM), CI is the process by which software development teams implement changes.

Fundamentally, [Scrum](https://www.scrum.org/resources/what-is-scrum){:target="_blank"} and other Agile methodologies are a management framework, not software engineering practices. Teams need to combine Agile frameworks with software engineering best practices to achieve the best results. Continuous integration puts Agile’s iterative development into action. 


# Agile core concepts

Agile project management delivers complex projects by focusing on incremental change. This iterative methodology has six core elements to track progress and create the product.

*   **User stories:** Describes the goals for the product written from the user’s perspective.
*   **Roadmap:** The high-level view of the requirements needed to fulfill the product vision.
*   **Backlog:** Requirements for the project by priority.
*   **Release plan:** A timetable for the release of a _working_ product.
*   **Sprint/Iteration:** The user stories, goals, and tasks linked to the current sprint.
*   **Increment:** The working product presented to the stakeholders at the end of a sprint.

Agile methodologies can vary in their approach but stay true to this model. In GitLab, project management features coincide with these Agile core concepts. 

|**Agile concept**     |**GitLab feature**                                                                                                                                                                   |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|User stories      |[Issues](https://docs.gitlab.com/ee/user/project/issues/)                                                                                                                        |
|Tasks             |[Task lists](https://docs.gitlab.com/ee/user/markdown.html#task-lists)                                                                                                           |
|Epics             |[Epics](https://docs.gitlab.com/ee/user/group/epics/)                                                                                                                            |
|Points/Estimations|[Weights](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html)                                                                                                      |
|Backlog           |[Issue lists](https://docs.gitlab.com/ee/user/project/issues/#issues-per-project) and [prioritized labels](https://docs.gitlab.com/ee/user/project/labels.html#prioritize-labels)|
|Sprints           |[Milestones](https://docs.gitlab.com/ee/user/project/milestones/)                                                                                                                |
|Burndown charts   |[Burndown charts](https://docs.gitlab.com/ee/user/project/milestones/burndown_charts.html)                                                                                       |
|Agile boards      |[Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html)   



# CI workflow in Agile

Git it is the most popular method of [version control](/topics/version-control/){:target="_blank"}, and can be beneficial for Agile teams because it allows for decentralized and simultaneous development. Tools like GitLab and others create a process around code mainly through issues and merge requests. 

A [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests){:target="_blank"} (MR) is a way to visualize and collaborate on proposed changes to source code. A [CI workflow](/topics/version-control/what-is-gitlab-workflow/){:target="_blank"} for teams using an Agile methodology would incorporate Agile core concepts as well as [commits](/blog/2018/06/07/keeping-git-commit-history-clean/){:target="_blank"}, merge requests, testing, and deployments.


### Using labels

Labels are a way for teams to filter and manage epics, issues, and merge requests. For a specific project, teams may have labels such as `Planning`, `In Progress`, `Staging`, `Reviewed`, etc with their own lists in the Issue Board. 

Teams can collaborate in the issues, and once next steps have been decided, a developer assigned to the issue might begin work and remove the label `Planning` and replace it with the label `In Progress`.


### Code and commit changes

The developer can reference the corresponding issue in his commit message. The developer then pushes his commits to a [feature branch](https://docs.gitlab.com/ee/topics/git/feature_branch_development.html){:target="_blank"} and creates a merge request. The code is built and tested, and a team member can review the code changes.


### Using Issue Boards between teams

Once one team finishes their review, they can remove the label `In Progress` and move the issue to the `Staging` Issue Board. This can alert the deployment team that the issue is ready.


### Deploying code

Once the MR has been approved, it can be merged into the main branch and the issue closed. 


# Why CI is essential for Agile development

[Continuous integration](/topics/ci-cd/){:target="_blank"} drives development teams to implement small, frequent changes and validate code against version control repositories. In fact, CI can be considered a [pillar](https://www.browserstack.com/guide/continuous-integration-with-agile){:target="_blank"} of the Agile process. CI is so widely used that CI and Agile are often used interchangeably. While the concepts similarly stress small, iterative changes, they are not the same.

Agile is built on the expectation that developers can deliver small incremental updates to a product or service. In practice, this will only be achieved if an organization commits to CI/CD automation. Agile frameworks allow teams to move quickly and work independently, but if the org does not actively use CI/CD, then Agile methods are essentially futile. 

Agile development strives to have a working product at the end of each Sprint. As the product goes through each Sprint, it’s improved exponentially. With CI, code changes made to the product are continually validated against other changes. Continuous integration and continuous testing ensures that teams can deliver this working product because bugs and potential issues are caught before they reach the end user.

## More on Agile development

[How to use GitLab for Agile software development](/blog/2018/03/05/gitlab-for-agile-software-development/){:target="_blank"}

[Burndown charts](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html){:target="_blank"}

[Milestones as Agile sprints](https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints){:target="_blank"}
