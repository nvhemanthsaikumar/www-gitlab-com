---
layout: job_family_page
title: "Risk and Field Security"
---

As members of GitLab's [Security Assurance sub department](/handbook/engineering/security/security-assurance/security-assurance.html), the [Risk and Field Security team](/handbook/engineering/security/security-assurance/risk-field-security/) serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers through customer support, risk management, sales enablement and security evangelism programs.

## Responsibilities
* Professionally handle communications with internal and external stakeholders
* Maintain up-to-date knowledge of GitLab's product, environment, systems and architecture
* Educate internal and external stakeholders on GitLab’s Security practices through formal and informal training, handbook improvements, white papers, conference presentations and blog posts
* Gather and report on established metrics within the risk and fireld security programs

## Requirements
* Ability to use GitLab
* Strong written and verbal communication and presentation skills
* Prior experience working with a SaaS company preferred

## Levels

### Risk and Field Security Engineer (Intermediate)
This position reports to the Manager, Risk and Field Security at GitLab.  

#### Risk and Field Security Engineer (Intermediate) Job Grade
The Risk and Field Security Engineer is a [6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Risk and Field Security Engineer (Intermediate) Responsibilities
* Complete customer security assessments, questionnaires and sales enablement activities
* Maintain the Customer Assurance Package and other self-service customer security resources
* Maintain GitLab's standard security response database (RFP)
* Support Risk Management activities including Third Party Vendor and Security Operational Risk assessments
* Triage new or changing security requirements, security issues, and/or Security Operational, Third Party or Customer risks
* Maintain handbook pages, policies, standards, procedures and runbooks related to Risk and Field Security
* Identify opportunities for Risk and Field Security process automation
* Maintain Risk and Field Security automation tasks

#### Risk and Field Security Engineer (Intermediate) Requirements
* At least 2 years of experience conducting customer support, security and/or risk management activities
* Demonstrated experience with common risk management standards and models such as: ISO 31000, NIST 800-39, FAIR, ISACA Risk IT, OCTAVE
* Demonstrated experience with at least two security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Working understanding of how security works with cloud-native technology stacks

### Senior Risk and Field Security Engineer
This position reports to the Manager, Risk and Field Security at GitLab.  

#### Senior Risk and Field Security Engineer Job Grade
The Senior Risk and Field Security Engineer is a [7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Risk and Field Security Engineer Responsibilities
* Extends the Risk and Field Security Engineer responsibilities
* Lead sales enablement activities, including customer security assessments and contract reviews
* Execute end to end Risk and Field Security initiatives in accordance with the compliance roadmap
* Mature the Customer Assurance Package and other self-service customer security resources
* Monitor industry trends and demands to position GitLab as an industry leader in Security and execute initiatives to support these trends
* Execute Risk Management activities including Third Party Vendor and Security Operational Risk Assessments
* Execute peer reviews and provide meaningful feedback 
* Design requirements for Risk and Field Security automation tasks
* Recommend new Risk and Field Security metrics and automate reporting of existing metrics

#### Senior Risk and Field Security Engineer Requirements
* Ability to use GitLab
* At least 5 years of experience conducting customer support, security and risk management activities
* Detailed experience with common risk management standards and models such as: ISO 31000, NIST 800-39, FAIR, ISACA Risk IT, OCTAVE
* Demonstrated experience with at least four security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Demonstrated industry security experience, particularly in DevSecOps, Application Security and/or Cloud-Native Security

### Staff Risk and Field Security Engineer
This position reports to the Manager, Risk and Field Security at GitLab.  

#### Staff Risk and Field Security Engineer Job Grade
The Staff Risk and Field Security Engineer is a [8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff Risk and Field Security Engineer Responsibilities
* Extends the Senior Risk and Field Security Engineer responsibilities
* Maintain expert knowledge of GitLab's product, environment, systems and architecture while mentoring others on this knowledge and helping to shape design decisions for the sake of security compliance efficiencies
* Mentor other Risk and Field Security Engineers and improve quality and quantity of the team's output
* Design and implement major iterations of Risk and Field Security programs in alignment with industry trends, predictions and customer demands
* Participate in Risk and Field Security roadmap development based on customer needs
* Present a minimum of 4 external facing engagements per annum, ex: Commit, conferences, guest speaking engagements, blog posts, whitepapers
* Create dynamic open-source Risk and Field Security programs that deliver value to the GitLab community
* Design, develop, and deploy scripts to automate administrative and process tasks related to Risk and Field Security
* Design, develop, and deploy an automated metric reporting for all Risk and Field Security programs

#### Staff Risk and Field Security Engineer Requirements
* At least 10 years of experience conducting customer support, security and risk management activities
* Expert experience with common risk management standards and models such as: ISO 31000, NIST 800-39, FAIR, ISACA Risk IT, OCTAVE
* Expert experience with at least six security control frameworks such as: SOC 2, ISO, NIST, COSO, COBIT
* Demonstrated industry security experience, particularly in DevSecOps, Application Security and/or Cloud-Native Security

### Manager, Risk and Field Security 
The Risk and Field Security Manager reports directly to the Security Assurance Director.

#### Manager, Risk and Field Security Job Grade
The Risk and Field Security Manager is a [8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Risk and Field Security Responsibilities
* Evangelize security across GitLab and with our customers
* Hire and oversee a world class team of security engineers and Engineers
* Build a strong, collaborative partnership with Security, Infrastructure, Sales and Product teams
* Maintain a dynamic operational risk management program
* Maintain a comprehensive risk-based vendor security management program
* Lead customer calls, contract reviews and/or assessments providing assurance on GitLab security
* Assess and promote customer security requests across the GitLab security and product teams
* Prepare and deliver meaningful metrics to Security Assurance leadership
* Identify and implement automation of manual processes to shorten review and request cycles
* Successfully execute on quarterly OKRs
 
#### Manager, Risk and Field Security Requirements
* Exceptional communication skills, including verbal, written, and presentation skills to a variety of stakeholders
* At least 3 years prior experience managing information security customer facing teams
* Detailed knowledge of common risk management standards and models such as: ISO 31000, NIST 800-39, FAIR, ISACA Risk IT, OCTAVE
* Working knowledge of common information security management frameworks, regulatory requirements and applicable standards such as: ISO 27001, SOC 2, HIPAA, GDPR, PCI, SOX, etc.

### Senior Manager, Risk and Field Security 
The Risk and Field Security Manager reports directly to the Security Assurance Director.

#### Senior Manager, Risk and Field Security Job Grade
The Risk and Field Security Senior Manager is a [9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Risk and Field Security Responsibilities
* Extends the Manager, Risk and Field Security responsibilities
* Create and deploy innovative and effective strategies for proactively addressing prospective and customer security requests
* Create and deploy innovative and effective strategies for managing security operational and vendor risk
* Maintain reliable, up-to-date, information across the industry regarding new security trends, threats and vulnerabilities
* Present a minimum of 6 external facing engagements per annum, ex: Commit, conferences, guest speaking engagements, blog posts, whitepapers
* Draft and successfully execute on quarterly OKRs
 
#### Senior Manager, Risk and Field Security Requirements
* At least 6 years prior experience managing information security customer facing teams
* Expert knowledge of common risk management standards and models such as: ISO 31000, NIST 800-39, FAIR, ISACA Risk IT, OCTAVE
* Detailed knowledge of common information security management frameworks, regulatory requirements and applicable standards such as: ISO 27001, SOC 2, HIPAA, GDPR, PCI, SOX, etc.

## Segment
### Security Leadership
For details on the Security organization leadership roles, to include the Security Assurance Director and VP of Security, see the Security Leadership page.

## Performance Indicators
* [Security Impact on ARR](https://about.gitlab.com/handbook/engineering/security/performance-indicators/#security-impact-on-iacv)

## Career Ladder
```mermaid
  graph LR;
  sec:se(Risk and Field Security Engineer)-->sec:sse(Senior Risk and Field Security Engineer);
  sec:sse(Senior Risk and Field Security Engineer)-->sec:stse(Staff Risk and Field Security Engineer);
  sec:sse(Senior Risk and Field Security Engineer)-->sec:sem(Manager, Risk and Field Security);
  sec:sem(Manager, Risk and Field Security)-->sec:sesm(Senior Manager, Risk and Field Security);
  sec:sesm(Senior Manager, Risk and Field Security Senior)-->sec:ds(Director, Security Assurance);
```

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 50-minute interviews with the hiring manager, 
* Then, candidates will be invited to schedule 3 separate 50-minute interviews with 3 different peers from within the Security orgnaization,
* Finally, candidates will be invited to schedule a 25-minute interview with the Director, Security Risk and Compliance.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
