---
layout: handbook-page-toc
title: "Community Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Our mission

GitLab's Community Operations Program is responsible for developing and maintaining the infrastructure and resources to support the Community Relations team and the GitLab community at large. In this, we help support GitLab's greater mission of 'everyone can contribute' by encouraging the wider GitLab community through process and opportunity.  

### Partnership

The mission of this program is to act as a partner to all Program Managers in the Community Relations team to define, implement and refine KPIs/PIs to measure and report the success and effectiveness of our community programs. We work together with the Community Relations team’s Program Managers to produce regular, engaging content to highlight their programs and attract new contributors.

[Community Relations Handbook](https://about.gitlab.com/handbook/marketing/community-relations/)

The Community Operations function also works closely with the Marketing Operations, and Data and Analytics teams.

### Tooling

Community Operations defines and maintains the tool stack required to measure and interact with the wider GitLab community.
The Community Program Manager acts as the DRI for the Community Relations team’s webpages on about.gitlab.com. This program also supports the Open Source and Education teams by processing program applications and renewals. Ultimately, we are working towards a process to fully automate this.

#### Community Operations Tool Stack

These are the tools the Community Relations team is the DRI for:

| Tool Name | Description | How We Use
|-------------|-------------|-----------|
| Bitergia | [Bitergia](gitlab.biterg.io/) is the platform we use to measure and track metrics related to contributing code and documentation to GitLab | [How we use Bitergia](/handbook/marketing/community-relations/code-contributor-program/#bitergia-dashboard) |
| Crowdin | [Crowdin](https://translate.gitlab.com/) is the platform for the wider community to collaboratively contribute translations for GitLab | [How we use Crowdin](https://docs.gitlab.com/ce/development/i18n/translation.html) |
| Discourse | [Discourse](https://www.discourse.org) is the platform on which the [GitLab forum](https://forum.gitlab.com) is run. | [How we use Discourse](/handbook/marketing/community-relations/community-operations/workflows/forum/#administration)|
| Disqus  | [Disqus](https://disqus.com) is the commenting on blog.gitlab.com and docs.gitlab.com | [How we use Disqus](/handbook/marketing/community-relations/community-operations/tools/disqus) |
| Gitter | [Gitter](https://gitter.im/gitlab/home) is the instant messaging platform the GitLab community communicates on (in addition to GitLab.com itself) | We currently have a `gitlab` community with 3 channels: `gitlab` (general conversation, questions about GitLab), `contributors` (chat and support about contributing to GitLab) and `heroes` (GitLab Heroes chat) |
| KeyHole | [KeyHole](https://keyhole.io) is the tool we use to collect Twitter impressions and YouTube views | [How we use KeyHole](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/metrics/#metrics-collections) |
| Meetup | [Meetup.com](https://www.meetup.com/pro/gitlab/) is the platform we use and offer to our community to organize meetups | [How we use Meetup.com](/handbook/marketing/community-relations/evangelist-program/#meetups) |
| SameRoom | [SameRoom](https://sameroom.io/) is the platform we use to bridge public Gitter channels with the private GitLab Slack instance | SameRoom enables bidirectional communication from Slack to Gitter and viceversa. Currently it is enabled for the `gitlab/contributors` Gitter channel and the reciprocal `#gitter-contributors-room` on Slack. Messages sent from Slack are forwarded to Gitter using the `gitter-badger` account, but otherwise they show the display name of the Slack user who sent the message.|
| SheerId | [SheerId](https://www.sheerid.com/) is the platform we use to automatically qualify applications to our community programs | |
| Zapier |  [Zapier](https://zapier.com) is an automation tool used to identify mentions and to route them into Zendesk as tickets, and also to Slack in some cases | [How we use Zapier](/handbook/marketing/community-relations/community-operations/tools/zapier) |
| Zendesk | [Zendesk](https://www.zendesk.com/support/) is the tool Community Ops, EDU & OSS work their program cases and applications  | [How we use Zendesk](/handbook/marketing/community-relations/community-operations/zendesk/)|

#### Other tools we rely on

These are the tools that are essential to some Community programs, but the Community Relations team are not the DRI for:

| Tool Name | Description | How We Use
|-------------|-------------|-----------|
| Customer Portal | [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) - Web portal where customers can manage their subscriptions and account information. | To help troubleshoot issues with community program applications |
| License Portal | [LicenseApp](/handbook/engineering/development/fulfillment/architecture/#licensedot) - Web portal to generate and manage GitLab licenses. | To create and manage licenses for community program applications and for [GitLab EE contributors](/handbook/marketing/community-relations/code-contributor-program/#contributing-to-the-gitlab-enterprise-edition-ee) |
| Marketo | [Marketo](/handbook/marketing/marketing-operations/marketo/) | Powers each intake form for our ([Education](/solutions/education/), [Open Source](/solutions/open-source/), and [Startups](/solutions/startups/)) programs. It is an integration which inserts the application record into Salesforce. |
| Printfection |[Printfection](https://www.printfection.com/) is our swag management platform  | [How we use Printfection](/handbook/marketing/corporate-marketing/merchandise-handling) |
| Salesforce  | [Salesforce](https://www.salesforce.com) is our [CRM](https://en.wikipedia.org/wiki/Customer_relationship_management) | We use Salesforce (SFDC) to [support the Education, Open Source and Startup Programs](/handbook/marketing/community-relations/community-operations/community-program-applications)   |

### GitLab's Community

The Community Operations Program curates and maintains documentation for any team member to productively engage with the wider community. When necessary, we engage with specialists within GitLab to provide responses and listen to our community’s feedback on [The GitLab forum](https://forum.gitlab.com), the [GitLab blog](https://about.gitlab.com/blog/) and Hackernews.

The [Community Operations Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) reports to the [Director of the Community Relations Team](https://about.gitlab.com/job-families/marketing/director-of-community-relations/).

## Community Operations Work

You can find the Community Operations Program peppered throughout the Community Relations handbook, processes, and projects.

In order to loop in Community Operations on GitLab.com, please use the `community-ops` label.

Everything labeled `community-ops` is organized on the [Community Operations Issue Board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/2062229?assignee_username=LindsayOlson&label_name[]=Community%20Ops).

### How to Use the Community Operations Issue Board

| Column Name | Description |
|-------------|-------------|
| Open        | All open issues with the `community-ops-` label   |
| Todo        | Issues with a due date or high priority   |
| Doing       | Issues that are currently in-flight (these have action items for the Community Operations Manager)   |
| Closed      | Issues that have been closed or completed   |

### How to Use the community-ops Label in GitLab

Please use the `community-ops` label only when there is an action item needed from the Community Operations Manager.

If you think an issue or MR is a "nice to know" for the Community Operations Manager, feel free to loop them in (@lindsayolson) via a comment instead.

### Community Relations Budget Operations

The Community Operations Manager is the DRI for the Community Relations Team budget. 

For workflows and processes, see the [Community Relations Team budget handbook page](/handbook/marketing/community-relations/#team-budgets). 


## Community Operations Response Channels

| Channel Name | Source      | Action.     |
|--------------|-------------|-------------|
| HackerNews    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| Hacker News front page stories    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| GitLab Forum    | Discourse (forum.gitlab.com)   | Find an expert in the Forum Contributors Group, and collaborate in Slack |
| GitLab Blog    | Disqus (blog.gitlab.com)   | Find an expert (usually the blog post author), and collaborate in Slack |

## Relevant Processes

It is not uncommon that the Social Media Team, and Communications Team at GitLab reaches out to Community Operations. Because of Community Operaions' response channels, and nature of the program, Community Operations has a unique view of the community's sentiment and tone. This means that the Community Operations Manager uses the following resources and handbook pages as needed, when partnering with Social and Comms to ensure a quality response back to our community in times of crisis or gerneral low sentiment and tone.

[Check out the social, community operations, and expert shared Community Management practices](/handbook/marketing/corporate-marketing/social-marketing/community-management/)

### Social Media Team

[Social Media Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)

### Expert Responses

Occasionally the Community Operations Manager will encourage GitLab Team Members, as experts, to engage with the wider GitLab community following our Team Member [Social Media Guidelines](/handbook/marketing/social-media-guidelines/) as well as these additional flexible guidelines, listed here:  

1. Experts are encouraged be themselves, while using the [GitLab Writing Style Guidelines](/handbook/communication/#writing-style-guidelines) - we always want to be personable and human in our online presence.
2. Engage as yourself whenever possible. (ex. use your personal Twitter handle)
3. Always try and link to relevant documentation, issues, handbook pages, forum topics and other resources.

#### Examples of when to involve an expert

- Technical questions or comments on a GitLab blog post
- HackerNews questions about a strategic GitLab topic (e.g. CI, security) or misinformation about GitLab
- Forum topics re: specific use cases not covered in the docs or relevant product feedback


#### Slack Template for Involving Experts

> @expert_username [LINK TO COMMUNITY COMMENT] Hello! An expert is needed to respond to this. Could you please answer on [name of social platform] using your own individual account?  If you don't know the answer, could you share your thoughts and ping a specific expert who might? Or if there is a more appropriate channel to ask, could you point me in that direction? Thanks!

### Forum Uses

#### Announcements

In the past the Product Management Team has sucessfully used the forum as a space to foster conversation and encourage feedback on changes to the GitLab product. [Here is an example of when we changed our subscription model.](https://forum.gitlab.com/t/new-gitlab-product-subscription-model/45923)

#### Announcement Components

#### Banner (optional)

Steps for bannering on Discourse
1. Download design component to your computer
2. In Discourse navigate to Admin Panel > Customize > Themes > Top Navbar
3. Under uploads, click the `Add+` button
4. Add your design 
5. Click `Edit CSS/HTML`
6. Add mini banner CSS:
   ```
   .mini-banner__image {
     height: 60.5px;
     width: 720px;
     background-image: url($mini-banner);
     background-size: 100% 100%;
     margin: 0 auto;
   }
   ```
7. Click `After Header and add this div to make CSS work`
   ```
   <div class="main-banner">
     <a href="https://forum.gitlab.com">
       <div class="main-banner__image"></div>
     </a>

     <a href="https://forum.gitlab.com/t/ci-cd-minutes-for-free-tier/40241" target="__blank">
       <div class="mini-banner__image"></div>
     </a>
   </div>
   ```
8. Slight modifications may be needed depending on the size of the image

#### Announcement Draft Post

Typically it is the task of the Product Manager or DRI to draft and wordsmith the announcement post. This will happen via an issue, so the forum DRI can simpy copy and paste the language into the forum draft verbatim. 

In order to visualize and prepare for the upcoming announcement do this in the [Staff category](https://forum.gitlab.com/c/staff/4) That way users won't be able to see the announcement before it's time, and the forum DRI will be able to add links to the Announcement's FAQ and/ or Blog post, as well as link to [GitLab's Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/). 

Steps for creating a draft post

1. Navigate to the [Staff category](https://forum.gitlab.com/c/staff/4)
2. Click `+New Topic`
3. Paste in content/language from issue
4. Add approved suject line (this will end up being the URL)
5. Click `Create topic`
6. Share screenshot of topic draft in related issed since most Product Managers do not have permissions to visit the Staff category. 

When it's time for the Draft post to "go live"

1. Click the big pencil next to the subject line
2. Choose appropriate category from the dropdown underneath the subject line

Optional steps are: 
1. Change the topic owner from yourself to the Product DRI by clicking on the wrench icon on the right of the screen and select from dropdown
2. Change the timestamp by clicking on the wrench icon on the right of the screen and select from dropdown

### Support for Education, Open Source and Startup Programs

Community Operations supports the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program) and [Startups](/solutions/startups) Programs, and their Program Managers.

The Community Operations team helps process and manage program applications as per the [community programs applications workflow](/handbook/marketing/community-relations/community-operations/community-program-applications).

#### Common Community Program metrics

Due to the similarity among our Community Programs' workflows and goals, the Community Operations team helps keep track of [common community program metrics](/handbook/marketing/community-relations/community-operations/workflows/metrics/). 
