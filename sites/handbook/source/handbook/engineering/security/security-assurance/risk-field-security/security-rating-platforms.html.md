---
layout: handbook-page-toc
title: Third Party Security Rating Platforms
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# BitSight

BitSight is a third party security rating platform that utilizes public information collected across multiple domains to provide a numeric score from 250-900 (similar to a credit rating, but security focused). GitLab publishes three reports using BitSight:
 
- GitLab Production- this report represents the security posture of any URL or IP associated with supporting our [production architecture](/handbook/engineering/infrastructure/production/architecture/). Our goal is to maintain a rating of 700 or higher. Alerts from BitSight are monitored in real time and addressed as part of our [Vulnerability Management](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/vulnerability-management.html) program.
- GitLab Sub-Production- this report represents the security posture of any URL or IP that does NOT support the Production Environment noted above. This generally includes test environments and/or environments that are purposely kept out-of-date for quality assurance activities. This score is not monitored by GitLab and does not represent GitLab's Security Posture.
- GitLab User-Managed- many of our customers utilize static pages as part of gitlab.io. These pages are managed solely by our customers. However, from time to time, these pages are associated with GitLab erroneously. As such, we have moved any page on the GitLab.io domain to this report. This score is not monitored by GitLab and does not represent GitLab's Security Posture.

# Security Scorecard
 
Security Scorecard monitors public information across 10 risk factors (such as DNS health, IP reputation, network security and patching cadence) and publicly reports an A-F rating. Gitlab does not monitor this score regularly. GitLab will review reported items sporadically with a goal of maintaining a B or higher.
