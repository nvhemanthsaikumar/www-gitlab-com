---
layout: handbook-page-toc
title: GitLab's Customer Assurance Package
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package

At GitLab, we believe that [transparency](/handbook/values/#transparency) is critical to our success- and security is no different. Our **Customer Assurance Package (CAP)** is designed to provide GitLab team members, users, customers, and other community members with the most current information about our Security and Compliance Posture.

## Self Service Resources

We encourage our Customers and Prospects to begin by reviewing our Security Assurance self-service resources below.

### GitLab's Information Security Policies

- [GitLab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
- [GitLab Password Policy](/handbook/security/#gitlab-password-policy-guidelines)
- [GitLab Access Management Policy](/handbook/engineering/security/#access-management-process)
- [GitLab Data Classification Policy](/handbook/engineering/security/data-classification-standard.html)
- [GitLab Data Protection Impact Assessment Policy](/handbook/engineering/security/dpia-policy/)
- [GitLab Penetration Testing Policy](/handbook/engineering/security/penetration-testing-policy.html)
- [GitLab Audit Logging Policy](/handbook/engineering/security/audit-logging-policy.html)
- [GitLab Security Incident Response Guide](/handbook/engineering/security/security-operations/sirt/sec-incident-response.html)
- [GitLab Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan/)

### GitLab Architecture

- [GitLab Application Architecture](https://docs.gitlab.com/ee/development/architecture.html)
- [GitLab Production Architecture](/handbook/engineering/infrastructure/production/architecture/)
- [High-level Network Diagram](/handbook/engineering/infrastructure/production/architecture/#network-architecture)
- [Blog Post: Securing your Instance Best Practices](/blog/2020/05/20/gitlab-instance-security-best-practices/)

### GitLab's Security Compliance

- [GitLab Security Trust Center](/security/)
- [GitLab Security Control Framework](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
- [GitLab's SOC 3 Report](https://gitlab.com/gitlab-com/gl-security/soc-3-project)
- [GitLab's SOC2 Type 2 Report request process](/handbook/engineering/security/security-assurance/security-compliance/certifications.html#requesting-a-copy-of-the-gitlab-soc2-type-2-report). **Non-Disclosure Agreement** required. 
- [GitLab's Annual Penetration test request process](/handbook/engineering/security/#external-contact-information). **Non-Disclosure Agreement** required. 

### Technical Reports

- [Securing Customer Data](/handbook/engineering/security/security-assurance/risk-field-security/technical-reports/securing-customer-data.pdf)

### Completed Questionnaires 

- [Cloud Security Alliance (CSA)
Consensus Assessments Initiative Questionnaire (CAIQ)](https://cloudsecurityalliance.org/star/registry/gitlab/) 

### Third Party Security Rating Platforms

#### BitSight

BitSight utilizes public information collected across multiple domains to provide a numeric score from 250-900. GitLab's Production Summary Report was last updated February 2021. For information on how GitLab maintains this score, refer to our [Third Party Security Rating Platforms](/handbook/engineering/security/security-assurance/risk-field-security/security-rating-platforms.html) handbook page.
 
![GitLab's BitSight Report](/handbook/engineering/security/security-assurance/images/BitSightFeb21.png)
 
## Additional Support

If you have any further questions that aren't answered here, please follow the below steps:

**Prospective Customers**: Please [fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you.

**Current Customers**: Please contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please reach out to [support@gitlab.com](mailto:support@gitlab.com) and ask to be connected to your Account Owner.

**GitLab Team Members**: Contact the Risk and Field Security team using the **Customer Assurance** workflow in the slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) channel. 

---
Coming soon: Regulated Markets Customer Assurance Package
